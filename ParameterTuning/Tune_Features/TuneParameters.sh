#!/bin/bash
#SBATCH -A cvit
#SBATCH -n 20
#SBATCH -p long
#SBATCH --mem=80000
#SBATCH -t 24:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=koustav.ghosal@research.iiit.ac.in

echo $1 $2 $3 $4 $5 $6 $7
#/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "gridSearchParam('$1','$2','$3','$4','$5','$6','$7');"
#/scratch/matlab/R2013b/bin/matlab
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment('/lustre/koustav/BMVC_15/structures/SelectedFeatures/','$1','$2','$3');"
