#!/bin/bash

#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "600" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "700" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "800" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "900" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "1000" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "1200" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "1300" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "1400" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "1500" "40" "40"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "10" "70"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "15" "65"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "20" "60"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "30" "50"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "50" "30"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "60" "20"
#sbatch TuneParameters.sh DSIFT /lustre/koustav/Datasets/caltech105_80.mat "500" "70" "10"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "4" "4"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "4" "8"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "4" "12"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "4" "16"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "8" "4"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "8" "8"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "8" "12"
sbatch TuneParameters.sh HOG /lustre/koustav/Datasets/caltech105_80.mat "500" "40" "40" "8" "16"

