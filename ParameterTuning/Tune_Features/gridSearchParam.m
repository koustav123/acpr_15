%function gridSearchParam(Feature,Path,CC,nTrain,nTest,HOG_CELL_SIZE,HOG_ORIENTATIONS,SOLVER)
 Feature = 'HOG';
 Path = '/lustre/koustav/Datasets/caltech105_80.mat';
 CC = '500';
 nTrain = '40';
 nTest = '50';
 HOG_CELL_SIZE = '8';
 HOG_ORIENTATIONS = '8';
 SOLVER = 'liblinear';

run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup');
%set parameters
conf.FeatureType = Feature;
conf.DatasetPath = Path;
conf.NumberOfWords = str2num(CC);
conf.NTrain = str2num(nTrain);
conf.NTest = str2num(nTest);
conf.modelPath = '/lustre/koustav/SVMmodel.mat';
conf.clobber = false ;
conf.svm.C = 10 ;
conf.svm.solver = SOLVER ;
conf.svm.biasMultiplier = 1 ;
conf.expId = datestr(now);
conf.HogCellSize = str2num(HOG_CELL_SIZE);
conf.HogOrientations = str2num(HOG_ORIENTATIONS);


%Load Image Database
ImageDataBase = load(conf.DatasetPath);
ImageData = ImageDataBase.imdb.images.data;
ImageLabels = ImageDataBase.imdb.images.names;

if strcmp(conf.FeatureType, 'DSIFT')
    descrs = {};
    conf.phowOpts = {'Verbose', 0, 'Sizes', 7, 'Step', 5} ;
    conf.numSpatialX = [2 4] ;
    conf.numSpatialY = [2 4] ;
    conf.quantizer = 'kdtree' ;
    descrs = {};
    for i =1:numel(ImageData(1,1,1,:))
        %disp(i);clc;
        % for i = 1:10
        im = standarizeImage(ImageData(:,:,:,i));
        
        [drop, descrs{i}] = vl_phow(im,conf.phowOpts{:});
        
    end
    descrs = single(cat(2, descrs{:}));
    vocab = vl_kmeans(descrs, conf.NumberOfWords,'algorithm', 'elkan', 'MaxNumIterations', 50);
    conf.vocab = vocab;
    conf.vocab = vocab ;
    if strcmp(conf.quantizer, 'kdtree')
        conf.kdtree = vl_kdtreebuild(vocab);
    end
    hists = {};
    for ii =1:numel(ImageData(1,1,1,:))
        %for ii = 1:10
        im = standarizeImage(ImageData(:,:,:,ii));
        hists{ii} = getImageDescriptor(conf, im);
    end
    %end
    %save('/lustre/koustav/histograms_caltech105_80.mat', 'hists','-v7.3') ;
    
elseif strcmp(conf.FeatureType, 'HOG')
    hists = {};
    for i =1:numel(ImageData(1,1,1,:))
        Image = standarizeImage(ImageData(:,:,:,i));
        lvl = graythresh(Image);
        Image = im2bw(Image,lvl);
        hog = vl_hog(single(Image), conf.HogCellSize,'numOrientations', conf.HogOrientations,'variant', 'dalaltriggs') ;
        hists{i} = hog(:);
    end
elseif strcmp(conf.FeatureType, 'CNN')
    %Hists = load('/lustre/koustav/histograms_caltech105_80.mat');
    %hists = Hists.hists;
end

Accuracy = checkAccuracyGeneral(conf, hists, ImageDataBase);
ResultToWrite = {conf.FeatureType, conf.DatasetPath, num2str(conf.NumberOfWords) ...
    num2str(conf.NTrain), num2str(conf.NTest),num2str(conf.HogCellSize),...
    num2str(conf.HogOrientations), num2str(Accuracy),conf.svm.solver};

fid=fopen('/home/cvit/koustav/results/ParameterTuning.csv','a');



fprintf(fid,'%s,',ResultToWrite{1,1:end-1})

fprintf(fid,'%s\n',ResultToWrite{1,end})

fclose(fid);
%end
