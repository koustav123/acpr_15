function [Precision, Recall] = retrieval(ImageProjectedFeatures,ImageProjectedLabels,SketchProjectedFeatures,SketchProjectedLabels)
%StructurePath = '/lustre/koustav/BMVC_15/structures/Fisher/';


% ImageProjectedData = load([StructurePath 'ImageSampleFeaturesProjected.mat']);
% ImageProjectedFeatures = ImageProjectedData.ImageSampleFeaturesProjected;
% ImageProjectedLabels = ImageProjectedData.ImageLabels;
% SketchProjectedData = load([StructurePath 'SketchTestFeaturesProjected.mat']);
% SketchProjectedFeatures = SketchProjectedData.SketchTestFeaturesProjected;
% SketchProjectedLabels = SketchProjectedData.SketchLabelsTest;

% ImageProjectedData = load([StructurePath 'ImageFeaturesNoCCA.mat']);
% ImageProjectedFeatures = ImageProjectedData.SketchFeaturesTrain;
% ImageProjectedLabels = ImageProjectedData.SketchLabelsTrain;
% SketchProjectedData = load([StructurePath 'SketchTestFeaturesNoCCA.mat']);
% SketchProjectedFeatures = SketchProjectedData.SketchFeaturesTest;
% SketchProjectedLabels = SketchProjectedData.SketchLabelsTest;



DatasetSize = size(ImageProjectedFeatures,1);
[ImageSampleCount,ImageUniqueLabel] = hist(ImageProjectedLabels,unique(ImageProjectedLabels));
%[IDX,D] = knnsearch(ImageProjectedFeatures,SketchProjectedFeatures,'k',DatasetSize,'distance','correlation');
[IDX,D] = knnsearch(ImageProjectedFeatures,SketchProjectedFeatures,'k',DatasetSize);
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    %disp(i);
    %thisSketchLabel = SketchLabels(i);
    %thisSketchLabel = find(SketchClass.Index == SketchLabels(i));
    thisSketchLabel = SketchProjectedLabels(i);
    %nSamples = sum(SketchLabels(:)==SketchLabels(i));
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (ImageProjectedLabels(IDX(i,j)) == thisSketchLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisSketchLabel == ImageUniqueLabel));
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr;RecallArray(i,:) = re;
end

Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
%[SortedAR,ids] = sort(AR);
%SortedAP = PrecisionArray(ids);
end
