function [ Wx,Wy ] = ReduceWeightMatrices( fold, eig )
%REDUCEWEIGHTMATRICES Summary of this function goes here
%   Detailed explanation goes here

data = load(['/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_CCA/' num2str(fold) '.mat']);
Cxx = data.Cxx;
Cyy = data.Cyy;
Cxy = data.Cxy;
M = data.M;
dx = data.dx;
dy = data.dy;

Cxx = Cxx/M; %+ 10^(lambda)*eye(size(dx));
Cyy = Cyy/M; %+ 10^(lambda)*eye(size(dy));
Cxy = Cxy/M;
Cyx = Cxy';
%save('/lustre/koustav/BMVC_15/structures/CNNFeatures/cov.mat','Cxx','Cyy','Cxy','Cyx');
%calculating the Wx cca matrix
try
   Rx = chol(Cxx);
catch
    disp('Matrix not positive definite. nearest positive definite matrix evaluated');
    Rx = chol(nearestSPD(Cxx));
end
%Rx = chol(nearestSPD(Cxx));
%Rx = chol(Cxx);
invRx = inv(Rx);
%Z = invRx'*Cxy*(Cyy\Cyx)*invRx;
Z = invRx'*Cxy*pinv(Cyy)*Cyx*invRx;
Z = 0.5*(Z' + Z);  % making sure that Z is a symmetric matrix
%[Wx,r] = eig(Z);   % basis in h (X)
[Wx,r] = svd(Z);
Wx = Wx(:,1:eig);
r = r(1:eig,1:eig);
%r = abs(r);
r = sqrt(real(r)); % as the original r we get is lamda^2
Wx = invRx * Wx;   % actual Wx values

% calculating Wy
%Wy = (Cyy\Cyx) * Wx;
Wy = pinv(Cyy)*Cyx* Wx;
% by dividing it by lamda
Wy = Wy./repmat(diag(r)',dy,1);

end

