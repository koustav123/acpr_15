function [ ] = Regularize( IpPath, OpPath,dataset,ImageDescriptor,SketchDescriptor,lambda )
%T Summary of this function goes here
%   Detailed explanation goes here

data = load([IpPath 'data.mat']);
ImageFeatures = data.ImageFeatures;
ImageLabels = data.ImageLabels;
partitions = data.partitions;


SketchFeatures = data.SketchFeatures;
SketchLabels = data.SketchLabels;

for i = 1:5
    if (i ==1)
        
        SketchLabelsTest = SketchLabels(partitions{1});
        
        SketchFeaturesTest = SketchFeatures(partitions{1},:);
        
    elseif (i ==2)
        
        SketchLabelsTest = SketchLabels(partitions{2});
        
        SketchFeaturesTest = SketchFeatures(partitions{2},:);
        
    elseif (i == 3)
        
        SketchLabelsTest = SketchLabels(partitions{3});
        
        SketchFeaturesTest = SketchFeatures(partitions{3},:);
        
    elseif (i == 4)
        
        SketchLabelsTest = SketchLabels(partitions{4});
        
        SketchFeaturesTest = SketchFeatures(partitions{4},:);
        
    else
        
        SketchLabelsTest = SketchLabels(partitions{5});
        
        SketchFeaturesTest = SketchFeatures(partitions{5},:);
        
    end
    [Wx,Wy] = FindWeightMatrices(i,str2num(lambda));
    SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
    ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
    [Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchLabelsTest);
end
AP = mean(Precision,1); AR = mean(Recall,1);
csvwrite([OpPath 'PR_Regularization_' num2str(lambda) '_' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[AP;AR]);
end
