#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 4
#SBATCH -p cvit
#SBATCH --mem=16000
#SBATCH -t 24:00:00

echo $1 $2 $3 $4
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Regularize('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_CCA/','/lustre/koustav/BMVC_15/Results/ParameterTuning/Tune_CCA/','$1','$2','$3','$4');"
