function [ Cxx,Cyy,Cxy,M,dx,dy ] = CreateCovariance( X,Y,XLabels,YLabels)
%CREATECOVARIANCE Summary of this function goes here
%   Detailed explanation goes here

if size(X,2) > size(Y,2)
    tempX = X;X = Y;Y = tempX;
    tempXLabels = XLabels; XLabels = YLabels; YLabels = tempXLabels;
    
end
[nx, dx] = size(X);
[ny, dy] = size(Y);

% Compute overall mean

vX = sqrt(var(X,1));
vY = sqrt(var(Y,1));
mX = mean(X,1);
mY = mean(Y,1);
X = X - repmat(mX, [nx 1]);
Y = Y - repmat(mY, [ny 1]);


Classes = (unique(XLabels,'rows'))'; % Number of different categories present


%------------------------compute covariance matrices------------------------------
%---------------------------------------------------------------------------------
Cxx = zeros(size(X,2),size(X,2));
Cyy = zeros(size(Y,2),size(Y,2));
Cxy = zeros(size(X,2),size(Y,2));
CntpairsC = [];
for i = Classes
	idx = find(XLabels == i);
    idy = find(YLabels == i);
	muXC = mean(X(idx,:));
	muYC = mean(Y(idy,:));
    %disp(size(muXC));
    %disp(size(muYC));
	CntpairsC = vertcat(CntpairsC,size(idx,1)*size(idy,1));
	
	Cxy = Cxy + (size(idx,1)*size(idy,1)*muXC'*muYC);
	for i1 = 1:size(idx,1)
		Cxx = Cxx + (size(idx,1)*(X(idx(i1),:))'*X(idx(i1),:));
    end
    for i2 = 1:size(idy,1)
		Cyy = Cyy + (size(idy,1)*(Y(idy(i2),:))'*Y(idy(i2),:));
    end
end
M = sum(CntpairsC);


end

