#!/bin/bash

sbatch Reg.sh Caltech CNN CNN -15
sbatch Reg.sh Caltech CNN CNN -14
sbatch Reg.sh Caltech CNN CNN -13
sbatch Reg.sh Caltech CNN CNN -12
sbatch Reg.sh Caltech CNN CNN -11
sbatch Reg.sh Caltech CNN CNN -10
sbatch Reg.sh Caltech CNN CNN 10
sbatch Reg.sh Caltech CNN CNN 12
sbatch Reg.sh Caltech CNN CNN 13
sbatch Reg.sh Caltech CNN CNN 14
sbatch Reg.sh Caltech CNN CNN 15

#sbatch Eig.sh Caltech CNN CNN 10
#sbatch Eig.sh Caltech CNN CNN 50
#sbatch Eig.sh Caltech CNN CNN 100
#sbatch Eig.sh Caltech CNN CNN 200
#sbatch Eig.sh Caltech CNN CNN 500
#sbatch Eig.sh Caltech CNN CNN 1000
#sbatch Eig.sh Caltech CNN CNN 1500
#sbatch Eig.sh Caltech CNN CNN 2000
#sbatch Eig.sh Caltech CNN CNN 2500
#sbatch Eig.sh Caltech CNN CNN 3000
#sbatch Eig.sh Caltech CNN CNN 3500
#sbatch Eig.sh Caltech CNN CNN 4000

