% clc;clear all;
% SketchLabels = [];SketchFeatures = [];
% mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
% 
% SketchDataPath = '/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/';
% SketchData=load([SketchDataPath 'TUB_CNN_Full_SketchData.mat']);
% TUBNames = SketchData.FileNames;
% TUBFeatures = SketchData.Features;
% 
% for i=1:size(mapping,1)
%     disp(i);
%     C=strsplit(mapping{i},':');
%     CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBFeatures(j,:)];
%     end
% end
% 
% 
% end
% %save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
% save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/SelectedFeatures/Caltech/TUB_CNN_Selected_Data.mat','SketchLabels','SketchFeatures','-v7.3');
% 
% 


clc;clear all;
SketchLabels = [];SketchFeatures = [];
mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
ClassMapping = importdata('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/TUBerlinmappingkoustavsir.txt');


SketchDataPath = '/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/';
SketchData=load([SketchDataPath 'TUB_CNN_Full_SketchData.mat']);
TUBNames = SketchData.FileNames;
TUBFeatures = SketchData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBNames == CL);
    SketchFeatures=[SketchFeatures;TUBFeatures(idx,:)];
    SketchLabels=[SketchLabels i*ones(size(TUBNames(idx)))];
% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBFeatures(j,:)];
%     end
% end


end
%save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/SelectedFeatures/Caltech/TUB_CNN_Selected_Data.mat','SketchLabels','SketchFeatures','-v7.3');