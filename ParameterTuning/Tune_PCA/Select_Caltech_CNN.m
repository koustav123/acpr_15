% clc;clear all;
% ImageLabels = [];ImageFeatures = [];
% mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
% 
% ImageDataPath = '/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/';
% ImageData=load([ImageDataPath 'Caltech_CNN_Full_ImageData.mat']);
% CaltechNames = ImageData.FileNames;
% CaltechFeatures = ImageData.Features;
% 
% for i=1:size(mapping,1)
%     C=strsplit(mapping{i},':');
%     CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end
% 
% 
% end
% %save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
% save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/SelectedFeatures/Caltech/Caltech_CNN_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');


clc;clear all;
ImageLabels = [];ImageFeatures = [];
mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
ClassMapping = importdata('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/caltechmappingkoustavsir.txt');

ImageDataPath = '/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/';
ImageData=load([ImageDataPath 'Caltech_CNN_Full_ImageData.mat']);
CaltechNames = ImageData.FileNames;
CaltechFeatures = ImageData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures=[ImageFeatures;CaltechFeatures(idx,:)];
    ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end


end
%save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/SelectedFeatures/Caltech/Caltech_CNN_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');