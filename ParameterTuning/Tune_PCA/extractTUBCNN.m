% clc;clear all;
% Features = load('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/sketchfeaturesfc6.mat'); Features = Features.featuresfc6;
% Labels = load('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/sketchnewlabels.mat');Labels = Labels.labels;
% mapping = importdata('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/TUBerlinmappingkoustavsir.txt');mapping = mapping.textdata;
% FileNames = cell([1,size(Labels,2)]);
% for i = 1: size(Labels,2)
%     FileNames{1,i} = mapping{Labels(1,i)};
% end
% %FileNames = FileNames.textdata;FileNames = FileNames';
% save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/TUB_CNN_Full_SketchData.mat','Features','FileNames','-v7.3');

clc;
clear all;
% Features = load('/lustre/koustav/BMVC_15/structures/FullData/caltech256featureswhitebackgroundnew.mat'); Features = Features.features;
% FileNames = importdata('/lustre/koustav/BMVC_15/structures/FullData/Caltech256FileList.txt');
% FileNames = FileNames.textdata;FileNames = FileNames';
% save('/lustre/koustav/BMVC_15/structures/FullFeatures/Caltech_CNN_Full_ImageData.mat','Features','FileNames','-v7.3');

imdb = load('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/tuberlinimdb.mat');imdb = imdb.imdb;
Features = imdb.images.features;
FileNames = imdb.images.labels;
%FileNames = FileNames.textdata;FileNames = FileNames';
save('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullFeatures/TUB_CNN_Full_SketchData.mat','Features','FileNames','-v7.3');
