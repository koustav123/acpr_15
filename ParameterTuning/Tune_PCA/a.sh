#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 4
#SBATCH -p cvit
#SBATCH --mem=16000
#SBATCH -t 72:00:00

echo $1 $2 $3 $4
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment_2('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/SelectedFeatures/','/lustre/koustav/BMVC_15/Results/ParameterTuning/Tune_PCA/','$1','$2','$3','$4');"

