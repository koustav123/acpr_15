clc;clear;
path = '/lustre/koustav/BMVC_15/Results/';
%path = '/lustre/koustav/BMVC_15/Results/ParameterTuning/Tune_PCA/';
FileList = dir([path 'PR_Pascal_*.csv']);FileList = {FileList.name};
for i =1:size(FileList,2)
    val = load([path FileList{i}]);
    plot(val(2,:),val(1,:));xlim([0 1]);ylim([0 1]);
    hold on;
    disp(FileList{i});
    disp(trapz(val(2,:),val(1,:))); 
end