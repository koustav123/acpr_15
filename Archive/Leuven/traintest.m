function traintest(varargin)

% only placeholders
opts.dataset = 'caltech101' ;
opts.prefix = 'bovw' ;
opts.encoderParams = {'numWords', 256} ;
opts.seed = 1 ;
opts.kernel = 'linear' ;
opts.dataDir = 'data';
opts.datasetSize = 6;
opts.numGroup1 = 2;
opts.numGroup2 = 2;
opts.numGroup3 = 2;
opts.numClasses = 16;
opts.whichStep = 1;
opts.datasetDir = 'data';
opts.resultDir = 'result';

for pass = 1:2
  opts.imdbPath = fullfile(opts.resultDir, 'imdb.mat') ;
  opts.encoderPath = fullfile(opts.resultDir, 'encoder.mat') ;
  opts.modelPath = fullfile(opts.resultDir, 'model.mat') ;
  opts.diaryPath = fullfile(opts.resultDir, 'diary.txt') ;
  opts.cacheDir = fullfile(opts.resultDir, 'cache') ;
  opts = vl_argparse(opts,varargin) ;
end

vl_xmkdir(opts.cacheDir) ;
diary(opts.diaryPath) ; diary on ;
disp('options:' ); 
disp(opts);
disp(opts.encoderParams);

% Get image database
if exist(opts.imdbPath)
  imdb = load(opts.imdbPath);
else
  switch(opts.whichStep)
    case 1
      imdb = setupGeneric1(opts.datasetDir, ...
        'numGroup1', opts.numGroup1, 'numGroup2', opts.numGroup2, 'numGroup3', opts.numGroup3,  ...
        'expectedNumClasses', opts.numClasses, ...
        'seed', opts.seed) ;
    case 2
      imdb = setupGeneric2(opts.datasetDir, ...
        'numGroup1', opts.numGroup1, 'numGroup2', opts.numGroup2, 'numGroup3', opts.numGroup3,  ...
        'expectedNumClasses', opts.numClasses, ...
        'seed', opts.seed) ;
    case 3
      imdb = setupGeneric3(opts.datasetDir, ...
        'numGroup1', opts.numGroup1, 'numGroup2', opts.numGroup2, 'numGroup3', opts.numGroup3,  ...
        'expectedNumClasses', opts.numClasses, ...
        'seed', opts.seed) ;
  end
 save(opts.imdbPath, '-struct', 'imdb') ;
end

switch(opts.whichStep)
  case 1
    trainNum = opts.numGroup1+opts.numGroup2;
    testNum = opts.numGroup3;
  case 2
    trainNum = opts.numGroup2+opts.numGroup3;
    testNum = opts.numGroup1;
  case 3
    trainNum = opts.numGroup1+opts.numGroup3;
    testNum = opts.numGroup2;
end

% Train encoder and encode images
if exist(opts.encoderPath)
  encoder = load(opts.encoderPath) ;
else
  train = vl_colsubset(find(imdb.images.set <= 2), 5000, 'uniform') ;
  %_SS_ 
  A = arrayfun(@(x) fullfile(imdb.imageDir,imdb.images.name{train(x)}), [1:numel(train)], 'UniformOutput', false);
  encoder = trainEncoder(A,opts.encoderParams{:});
%_SS_   encoder = trainEncoder(fullfile(imdb.imageDir,imdb.images.name(train)), ...
%_SS_                          opts.encoderParams{:});
  save(opts.encoderPath, '-struct', 'encoder');
  diary off ;
  diary on ;
end
  B = arrayfun(@(x) fullfile(imdb.imageDir,imdb.images.name{x}), [1:numel(imdb.images.name)], 'UniformOutput', false);
  descrs = encodeImage(encoder,B,'cacheDir', opts.cacheDir);
%descrs = encodeImage(encoder, fullfile(imdb.imageDir, imdb.images.name), ...
 % 'cacheDir', opts.cacheDir) ;

diary off ;
diary on ;

% Train and evaluate models
if isfield(imdb.images, 'class')
  classRange = unique(imdb.images.class) ;
else
  classRange = 1:numel(imdb.classes.imageIds) ;
end
numClasses = numel(classRange) ;

descrs = bsxfun(@times, descrs, 1./sqrt(sum(descrs.^2))) ;

% train and test
train = find(imdb.images.set <= 2) ;
test = find(imdb.images.set == 3) ;
lambda = 1 / (10*numel(train)) ;
par = {'Solver', 'sdca', 'Verbose', ...
       'BiasMultiplier', 1, ...
       'Epsilon', 0.001, ...
       'MaxNumIterations', 100 * numel(train)} ;

scores = cell(1, numel(classRange)) ;
ap = zeros(1, numel(classRange)) ;
ap11 = zeros(1, numel(classRange)) ;
w = cell(1, numel(classRange)) ;
b = cell(1, numel(classRange)) ;

for c = 1:numel(classRange)
  
  if isfield(imdb.images, 'class')
    y = 2 * (imdb.images.class == classRange(c)) - 1 ;
  else
    y = - ones(1, numel(imdb.images.id)) ;
    [~,loc] = ismember(imdb.classes.imageIds{classRange(c)}, imdb.images.id) ;
    y(loc) = 1 - imdb.classes.difficult{classRange(c)} ;
  end
  if all(y <= 0), continue ; end
    
  [w{c},b{c}] = vl_svmtrain(descrs(:,train), y(train), lambda, par{:}) ;
  scores{c} = w{c}' * descrs + b{c} ;

 [recall, precision, info] = vl_pr(y(test), scores{c}(test)) ;
  
  %figure;
  %plot(recall, precision);
  %title(imdb.meta.classes{classRange(c)})
  %vl_printsize(1) ;
  %print('-dpdf', fullfile(opts.resultDir, ['pr-' imdb.meta.classes{classRange(c)} '.pdf'])) ;

  %figure;
  %colormap(gray(256));
  %title(imdb.meta.classes{classRange(c)})
%   for t = 1:trainNum
%     subplot(ceil(sqrt(trainNum)), ceil(sqrt(trainNum)), t);
%     index = train(t + (c-1)*trainNum);
%     score = scores{c}(index);
%     score_str = sprintf('%f', score);
%     f = fullfile(imdb.imageDir, imdb.images.name{index});
%     image(imread(f));
%     axis off 
%     title(score_str);
%   end
%   vl_printsize(1) ;
  %print('-dpdf', fullfile(opts.resultDir, ['pr-' imdb.meta.classes{classRange(c)} '-train.pdf'])) ;
  
%   figure;
%   colormap(gray(256));
%   for t = 1:testNum
%     subplot(ceil(sqrt(testNum)), ceil(sqrt(testNum)), t);
%     index = test(t + (c-1)*testNum);
%     score = scores{c}(index);
%     score_str = sprintf('%f', score);
%     f = fullfile(imdb.imageDir, imdb.images.name{index});
%     image(imread(f));
%     axis off 
%     title(score_str);
%   end
%   vl_printsize(1) ;
  %print('-dpdf', fullfile(opts.resultDir, ['pr-' imdb.meta.classes{classRange(c)} '-test.pdf'])) ;

%   ap(c) = info.ap ;
%   ap11(c) = info.ap_interp_11 ;

end
scores = cat(1,scores{:}) ;

diary off ;
diary on ;
close all;
% confusion matrix (can be computed only if each image has only one label)
if isfield(imdb.images, 'class')
  [~,preds] = max(scores, [], 1) ;
  confusion = zeros(opts.numClasses) ;
  for c = 1:opts.numClasses
    sel = find(imdb.images.class == classRange(c) & imdb.images.set == 3) ;
    tmp = accumarray(preds(sel)', 1, [opts.numClasses 1]) ;
    tmp = tmp / max(sum(tmp),1e-10) ;
    confusion(c,:) = tmp(:)' ;
  end
else
  confusion = NaN ;
end

% save results
save(opts.modelPath, 'w', 'b') ;
save(fullfile(opts.resultDir,'result.mat'), ...
     'scores', 'ap', 'ap11', 'confusion', 'classRange', 'opts') ;

% figures
meanAccuracy = sprintf('mean accuracy: %f\n', mean(diag(confusion)));
mAP = sprintf('mAP: %.2f %%; mAP 11: %.2f', mean(ap) * 100, mean(ap11) * 100) ;

figure; clf ;
imagesc(confusion); 
colormap(gray);
axis square;
title([opts.prefix ' - ' meanAccuracy]) ;
vl_printsize(1) ;
print('-dpdf', fullfile(opts.resultDir, 'result-confusion.pdf')) ;

figure; clf ; bar(ap * 100) ;
title([opts.prefix ' - ' mAP]) ;
ylabel('AP %%') ; xlabel('class') ;
grid on ;
vl_printsize(1) ;
ylim([0 100]) ;
print('-dpdf', fullfile(opts.resultDir,'result-ap.pdf')) ;

disp(meanAccuracy) ;
disp(mAP) ;
diary off ;

end
