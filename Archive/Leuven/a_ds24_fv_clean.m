function a_ds24_fv_clean(datasetSize, useSpatialPyramid, whichStep, vlFeatPath, ipDir, opDir)
%function a_ds24_fv_clean()
%datasetSize=6;
%useSpatialPyramid=1;
%whichStep=1;
%vlFeatPath='/Pulsar2/koustav.ghosal/Leuven/vlfeat-0.9.20/toolbox';
%ipDir='/Pulsar2/koustav.ghosal/Leuven/';
%opDir='/Pulsar2/koustav.ghosal/Leuven/results';
%vl_setup_condor();
%addpath('/home/koustav/Desktop/Leuven/vlfeat-0.9.20/toolbox');
addpath(vlFeatPath);
vl_setup();

conf.dataset = 'sketches';
%conf.datasetDir = fullfile('/home/koustav/Desktop/Leuven/', [conf.dataset '_png']);
conf.datasetDir = fullfile(ipDir, [conf.dataset '_png']);
conf.datasetSize = str2num(datasetSize);
conf.numGroup1 = round(conf.datasetSize/3);
conf.numGroup2 = round(conf.datasetSize/3);
conf.numGroup3 = conf.datasetSize - conf.numGroup1 - conf.numGroup2;
conf.numClasses = 160;

% for 3-fold cross-validation
conf.whichStep = str2num(whichStep);
conf.spatialPyr = str2num(useSpatialPyramid);

conf.seed = 1 ;

if(conf.spatialPyr)
  conf.opts = {...
    'numWords', 256, ...
    'layouts', {'1x1', '2x2'}, ...
    'numPcaDimensions', 80 ...
    };
else
  conf.opts = {...
    'numWords', 256, ...
    'layouts', {'1x1'}, ...
    'numPcaDimensions', 80 ...
    };
end

folder = [conf.dataset '-ds24-fv-' num2str(conf.datasetSize) '-' num2str(conf.whichStep)];
if(conf.spatialPyr)
  folder = [folder '-sp'];
end
%conf.outputDir = fullfile('/home/koustav/Desktop/Leuven/results/', folder);
conf.outputDir = fullfile(opDir, folder);

traintest('seed', conf.seed, ...
          'dataset', conf.dataset, ...
          'datasetDir', conf.datasetDir, ...
          'datasetSize', conf.datasetSize, ...
          'resultDir', conf.outputDir, ...
          'numGroup1', conf.numGroup1, ...
          'numGroup2', conf.numGroup2, ...
          'numGroup3', conf.numGroup3, ...
          'numClasses', conf.numClasses, ...
          'whichStep', conf.whichStep, ...
          'encoderParams', conf.opts);

end
