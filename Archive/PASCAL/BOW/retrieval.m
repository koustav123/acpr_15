clc;clear all;
StructurePath = '';
ImageData = load([StructurePath 'ImageSampleFeaturesProjected.mat']);
ImageFeatures = ImageData.ImageSampleFeaturesProjected;
ImageLabels = ImageData.ImageSampleLabels;
[~,ImageLabels] = max(ImageLabels,[],2);
SketchData = load([StructurePath 'SketchTestFeaturesProjected.mat']);
SketchFeatures = SketchData.SketchTestFeaturesProjected;
SketchLabels = SketchData.SketchCategoriesTest;
SketchClass = SketchData.SketchClass;
DatasetSize = size(ImageFeatures,1);
[ImageSampleCount,ImageUniqueLabel] = hist(ImageLabels,unique(ImageLabels));
[IDX,D] = knnsearch(ImageFeatures,SketchFeatures,'k',DatasetSize);
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    %thisSketchLabel = SketchLabels(i);
    %thisSketchLabel = find(SketchClass.Index == SketchLabels(i));
    thisSketchLabel = SketchLabels(i);
    %nSamples = sum(SketchLabels(:)==SketchLabels(i));
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (ImageLabels(IDX(i,j)) == thisSketchLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(thisSketchLabel);
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr;RecallArray(i,:) = re;
end

AP = mean(PrecisionArray,1);AR = mean(RecallArray,1);
[SortedAR,ids] = sort(AR);
SortedAP = PrecisionArray(ids);