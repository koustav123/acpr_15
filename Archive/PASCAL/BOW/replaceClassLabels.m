function[SketchCategoriesTrain,SketchCategoriesTest] = replaceClassLabels(SketchCategoriesTrain,SketchCategoriesTest,Index)
for i=1:size(Index,2)
    SketchCategoriesTrain(find(SketchCategoriesTrain==Index(i)))=i;
    SketchCategoriesTest(find(SketchCategoriesTest==Index(i)))=i;
    end
end