function[Wx Wy] = clusterCCA(X,Y,cattr)

[nx, dx] = size(X);
[ny, dy] = size(Y);

% Compute overall mean

vX = sqrt(var(X,1));
vY = sqrt(var(Y,1));
mX = mean(X,1);
mY = mean(Y,1);
X = X - repmat(mX, [nx 1]);
Y = Y - repmat(mY, [ny 1]);


Classes = (unique(cattr,'rows'))'; % Number of different categories present


%------------------------compute covariance matrices------------------------------
%---------------------------------------------------------------------------------
Cxx = zeros(size(X,2),size(X,2));
Cyy = zeros(size(Y,2),size(Y,2));
Cxy = zeros(size(X,2),size(Y,2));
CntpairsC = [];
for i = Classes
	idx = find(cattr == i);
	muXC = mean(X(idx,:));
	muYC = mean(Y(idx,:));
	CntpairsC = vertcat(CntpairsC,size(idx,1)*size(idx,1));
	
	Cxy = Cxy + (size(idx,1)*size(idx,1)*muXC'*muYC);
	for i1 = 1:size(idx,1)
		Cxx = Cxx + (size(idx,1)*(X(idx(i1),:))'*X(idx(i1),:));
		Cyy = Cyy + (size(idx,1)*(Y(idx(i1),:))'*Y(idx(i1),:));
    end
end
M = sum(CntpairsC);
Cxx = Cxx/M;
Cyy = Cyy/M;
Cxy = Cxy/M;
Cyx = Cxy';

%calculating the Wx cca matrix
Rx = chol(Cxx);
invRx = inv(Rx);
%Z = invRx'*Cxy*(Cyy\Cyx)*invRx;
Z = invRx'*Cxy*inv(Cyy)*Cyx*invRx;
Z = 0.5*(Z' + Z);  % making sure that Z is a symmetric matrix
[Wx,r] = eig(Z);   % basis in h (X)
r = abs(r);
r = sqrt(real(r)); % as the original r we get is lamda^2
Wx = invRx * Wx;   % actual Wx values

% calculating Wy
Wy = (Cyy\Cyx) * Wx;

% by dividing it by lamda
Wy = Wy./repmat(diag(r)',dy,1);
end