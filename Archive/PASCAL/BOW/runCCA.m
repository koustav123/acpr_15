function [ U,V,r ] = runCCA(ImageFeatures,ImageLabels,SketchFeatures,SketchLabels)
%RUNCCA Summary of this function goes here
%   Detailed explanation goes here
%[U,V] = createCorrespondence(ImageFeatures,ImageLabels,SketchFeatures,SketchLabels);
G1 = load('/lustre/koustav/BMVC_15/structures/U.mat'); G2 = load('/lustre/koustav/BMVC_15/structures/V.mat');
X = G1.U;Y = G2.V;
X=X(2:1001,:);Y=Y(2:1001,:);
%save('correspondence.mat','U','V');
[U,V,r] = cca(X*X',Y*Y');
%[P,Q,r,U,V,stats] = canoncorr(X,Y);
end

function [X,Y] = createCorrespondence(ImageFeatures,ImageLabels,SketchFeatures,SketchLabels)
SketchClass.Index = [1 22 87 175 247 34 46 49 51 60 216 67 107 135 153 161 186 59 232 238];
SketchClass.Label = cell({'airplane','bicycle','flying-bird','sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted-plants','sheep','sofa','train','tv'});

X=zeros(1,size(SketchFeatures,2));Y=zeros(1,size(ImageFeatures,2));
for i=1:20
    disp(SketchClass.Label(i));
    tempPartition = ImageLabels(:,i) == 1;
    tempI = ImageFeatures(tempPartition,1:end);
    tempPartition = SketchLabels == SketchClass.Index(i);
    tempS = SketchFeatures(tempPartition,1:end);
    for j=1:size(tempS,1)
        X = [X;repmat(tempS(j,:),size(tempI,1),1)];
        Y = [Y;tempI];
    end
end
end

function [Wx, Wy, r] = cca(X,Y)

% CCA calculate canonical correlations
%
% [Wx Wy r] = cca(X,Y) where Wx and Wy contains the canonical correlation
% vectors as columns and r is a vector with corresponding canonical
% correlations.
%
% Update 31/01/05 added bug handling.

if (nargin ~= 2)
  disp('Inocorrect number of inputs');
  help cca;
  Wx = 0; Wy = 0; r = 0;
  return;
end


% calculating the covariance matrices
z = [X; Y];
C = cov(z.');
sx = size(X,1);
sy = size(Y,1);
Cxx = C(1:sx, 1:sx) + 10^(10)*eye(sx);
Cxy = C(1:sx, sx+1:sx+sy);
Cyx = Cxy';
Cyy = C(sx+1:sx+sy,sx+1:sx+sy) + 10^(10)*eye(sy);
%Cxx = nearestSPD(Cxx);
%calculating the Wx cca matrix
Rx = chol(Cxx);
invRx = inv(Rx);
Z = invRx'*Cxy*(Cyy\Cyx)*invRx;
Z = 0.5*(Z' + Z);  % making sure that Z is a symmetric matrix
[Wx,r] = eig(Z);   % basis in h (X)
r = sqrt(real(r)); % as the original r we get is lamda^2
Wx = invRx * Wx;   % actual Wx values

% calculating Wy
Wy = (Cyy\Cyx) * Wx; 

% by dividing it by lamda
Wy = Wy./repmat(diag(r)',sy,1);
end