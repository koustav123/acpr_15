%clc;clear all;
ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
SketchDataPath = '/lustre/koustav/BMVC_15/structures/HOGFeatures/TuBerlin/';
%TUBData = load([SketchDataPath 'TuB_hog_imdb.mat']);
TUBerlinFeatures = TUBData.features;TUBLabels = TUBData.labels.labels;
mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
fileID = fopen('/home/cvit/koustav/Datasets/howDoHumansSkOFeaturesBOW/features_matlab/map_id_label.txt');
SketchClassIndex = textscan(fileID,'%f %s','Delimiter',',');
fclose(fileID);
ImageDataPath = '/lustre/koustav/BMVC_15/structures/BOW/';
%ImageData=load([ImageDataPath 'imdb.mat']);
CaltechNames = ImageData.FileNames;
CaltechFeatures = ImageData.Features;
% for k =1:size(TUBLabels)
%     TUBNames{k} = SketchClassIndex{1,2}{TUBLabels(k),1};
% end
for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end

for j=1:size(TUBLabels,2)
    if ~isempty(findstr(TUBLabels{j},TUBLabel))
        SketchLabels=[SketchLabels;i];
        SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
    end
end

end

%save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/BMVC_15/structures/HOGFeatures/TuBerlin/SketchData.mat','SketchFeatures','SketchLabels');

% ImageData = load('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat');
% SketchData = load('/lustre/koustav/BMVC_15/structures/BOW/SketchData.mat');
% ImageFeatures = ImageData.ImageFeatures;
% ImageLabels = ImageData.ImageLabels;
% SketchFeatures = SketchData.SketchFeatures;
% SketchLabels = SketchData.SketchLabels;
% idx1=randperm(size(ImageLabels,1));
% ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
% 
% 
% partitions = getPartition(SketchLabels,5);
% SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{4})];
% SketchLabelsTest = SketchLabels(partitions{5});
% SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
% SketchFeaturesTest = SketchFeatures(partitions{5},:);
% idx2=randperm(size(SketchLabelsTrain,1));
% SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);
% 
% % %[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain(1:1200,:),ImageFeatures(1:1200,:),SketchLabelsTrain(1:1200,:),ImageLabels(1:1200,:));
% [Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain,ImageLabels);
% SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
% ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
% save('/lustre/koustav/BMVC_15/structures/BOW/SketchTestFeaturesProjected.mat','SketchTestFeaturesProjected','SketchLabelsTest');
% save('/lustre/koustav/BMVC_15/structures/BOW/ImageSampleFeaturesProjected.mat','ImageSampleFeaturesProjected','ImageLabels');
% 
