clc;clear all;
run('/home/koustav123/Koustav/vlfeat-0.9.20/toolbox/vl_setup');
DataSetPath = '/home/koustav123/Ameya/256_ObjectCategories/';
ClassDirList = dir(DataSetPath);isub = [ClassDirList(:).isdir];ClassDirList = {ClassDirList(isub).name};ClassDirList =ClassDirList(3:258);
features = [];labels = {}; fc = 1;
for i = 1:size(ClassDirList,2)
   FileList = dir([DataSetPath ClassDirList{i} '/*.jpg']);
   disp(ClassDirList{i});
   for j = 1:size(FileList,1)
       Image = imread([DataSetPath ClassDirList{i} '/' FileList(j).name]);
       lvl = graythresh(Image);
       Image = imresize(im2bw(Image,lvl),[200 200]);
       hog = vl_hog(single(Image), 4,'numOrientations', 16,'variant', 'dalaltriggs') ;
       %imhog = vl_hog('render',hog,'verbose', 'variant', 'dalaltriggs','numOrientations', 16);
       %imshow(imhog);
       features(fc,:) = hog(:);
       labels{fc} = [ClassDirList{i} '/' FileList(j).name];
       fc = fc + 1;
   end
   save(strcat('/home/koustav123/Koustav/structures/caltech/',ClassDirList{i},'_caltech_hog_imdb.mat'),'features','labels');
   features = []; labels = []; fc = 1;
   
end
%save('/home/koustav123/Koustav/structures/caltech/caltech_hog_imdb.mat','features','labels');
