function calculateDim(nEig) 
ImageData = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/ImageData.mat');
SketchData = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/SketchData.mat');
ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;
SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;
idx1=randperm(size(ImageLabels,1));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);


partitions = getPartition(SketchLabels,5);
SketchLabelsTrain = [SketchLabels(partitions{5});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{4})];
SketchLabelsTest = SketchLabels(partitions{1});
SketchFeaturesTrain = [SketchFeatures(partitions{5},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
SketchFeaturesTest = SketchFeatures(partitions{1},:);
idx2=randperm(size(SketchLabelsTrain,1));
SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);

[nx, dx] = size(SketchFeaturesTrain);
[ny, dy] = size(ImageFeatures);

CovData = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/cov.mat');
Cxx = CovData.Cxx;
Cyy = CovData.Cyy;
Cxy = CovData.Cxy;
Cyx = CovData.Cyx;
Rx = chol(Cxx);
invRx = inv(Rx);
%Z = invRx'*Cxy*(Cyy\Cyx)*invRx;
Z = invRx'*Cxy*inv(Cyy)*Cyx*invRx;
Z = 0.5*(Z' + Z);  % making sure that Z is a symmetric matrix
[Wx,r] = eig(Z);   % basis in h (X)

Wx = Wx(:,nEig:end);
r = r(nEig:end,nEig:end);
r = abs(r);
r = sqrt(real(r)); % as the original r we get is lamda^2
Wx = invRx * Wx;   % actual Wx values

% calculating Wy
Wy = (Cyy\Cyx) * Wx;

% by dividing it by lamda
Wy = Wy./repmat(diag(r)',dy,1);
SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';







ImageProjectedFeatures = ImageSampleFeaturesProjected;
ImageProjectedLabels = ImageLabels;
SketchProjectedFeatures = SketchTestFeaturesProjected;
SketchProjectedLabels = SketchLabelsTest;
DatasetSize = size(ImageProjectedFeatures,1);
[ImageSampleCount,ImageUniqueLabel] = hist(ImageProjectedLabels,unique(ImageProjectedLabels));
[IDX,D] = knnsearch(ImageProjectedFeatures,SketchProjectedFeatures,'k',DatasetSize);
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    %thisSketchLabel = SketchLabels(i);
    %thisSketchLabel = find(SketchClass.Index == SketchLabels(i));
    thisSketchLabel = SketchProjectedLabels(i);
    %nSamples = sum(SketchLabels(:)==SketchLabels(i));
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (ImageProjectedLabels(IDX(i,j)) == thisSketchLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(thisSketchLabel);
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr;RecallArray(i,:) = re;
end

AP = mean(PrecisionArray,1);AR = mean(RecallArray,1);
plot(AR,AP,'b');xlim([0 1]);ylim([0 1]);title(strcat('PR',num2str(nEig)));
print(strcat('PR',num2str(nEig)),'-dpng');
end