function [ IDX_ARRAY ] = getPartition( labels,fold )
labels = labels(1:size(labels,1)-mod(size(labels,1),fold));
IDX_ARRAY=cell([fold 1]);
classes = unique(labels);
for c=1:size(classes);
    CIdx = find(labels == classes(c));
    CSize = size(CIdx,1);
    partitionsize = floor(CSize/fold);
    IDX_ARRAY_TEMP = mat2cell(CIdx,repmat(partitionsize,fold,1),1);
    for i=1:fold
        IDX_ARRAY{i}=[IDX_ARRAY{i}; IDX_ARRAY_TEMP{i}];
    end
end

end

