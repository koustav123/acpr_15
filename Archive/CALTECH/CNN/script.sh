#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 4  
#SBATCH -p cvit 
#SBATCH --mem=16000
#SBATCH -t 24:00:00

#/scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "TrainingDatasetProcessing('$1')"
#/scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "TrainingDatasetProcessingDepths('$1')"
#/scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "TrainingDatasetProcessing_kmeans('$1')"
#/scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "TrainingDatasetProcessing_kmeans_vlfeat('$1')"
#/scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "TrainingDatasetProcessingDepthsXY('$1')"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -logfile "/home/cvit/saurabh/WORK/DA_DepthEstimation/log_Sept16/log_'$1'.txt" -singleCompThread -r "TrainingDatasetProcessingDepthsSIFT('$1')"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -logfile "/home/cvit/saurabh/WORK/DA_DepthEstimation/log_Sept14/log_'$1'.txt" -singleCompThread -r "TrainingDatasetProcessingImageSIFT('$1')"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -logfile "/home/cvit/saurabh/WORK/DA_DepthEstimation/log_Sep22_image/log_'$1'.txt" -singleCompThread -r "TrainingDatasetProcessingImagesSIFT_kmeans('$1')"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -logfile "/home/cvit/saurabh/WORK/DA_DepthEstimation/log_Sept19/log_'$1'.txt" -singleCompThread -r "TrainingDatasetProcessingDepthsSIFT_kmeans('$1')"
LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2011a/bin/matlab -nodesktop -nosplash -singleCompThread -r "surfaceLabelDA('$1')"

