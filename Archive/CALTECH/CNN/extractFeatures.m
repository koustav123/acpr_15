clc;clear all;
%CaltechFeatures = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/caltech256imdb.mat');
%TUBerlinFeatures = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/tuberlinimdb.mat');
% mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
% ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
% CaltechNames = CaltechFeatures.imdb.images.names;
% TUBNames = TUBerlinFeatures.imdb.images.names;
% for i=1:size(mapping,1)
%     C=strsplit(mapping{i},':');
%     CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures.imdb.images.features(j,:)];
%     end
% end
% 
% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures.imdb.images.features(j,:)];
%     end
% end
% 
% end
% save('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/ImageData.mat','ImageFeatures','ImageLabels');
% save('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/SketchData.mat','SketchFeatures','SketchLabels');
ImageData = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/ImageData.mat');
SketchData = load('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/SketchData.mat');
ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;
SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;
idx1=randperm(size(ImageLabels,1));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);


partitions = getPartition(SketchLabels,5);
SketchLabelsTrain = [SketchLabels(partitions{5});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{4})];
SketchLabelsTest = SketchLabels(partitions{1});
SketchFeaturesTrain = [SketchFeatures(partitions{5},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
SketchFeaturesTest = SketchFeatures(partitions{1},:);
idx2=randperm(size(SketchLabelsTrain,1));
SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);

% %[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain(1:1200,:),ImageFeatures(1:1200,:),SketchLabelsTrain(1:1200,:),ImageLabels(1:1200,:));
[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain,ImageLabels);
SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
save('/lustre/koustav/BMVC_15/structures/CNNFeatures/SketchTestFeaturesProjected.mat','SketchTestFeaturesProjected','SketchLabelsTest');
save('/lustre/koustav/BMVC_15/structures/CNNFeatures/ImageSampleFeaturesProjected.mat','ImageSampleFeaturesProjected','ImageLabels');
% save('/lustre/koustav/BMVC_15/structures/CNNFeatures/SketchTestFeaturesNoCCA.mat','SketchFeaturesTest','SketchLabelsTest');
% save('/lustre/koustav/BMVC_15/structures/CNNFeatures/ImageFeaturesNoCCA.mat','SketchFeaturesTrain','SketchLabelsTrain');