data = load('/lustre/koustav/BMVC_15/structures/Fisher/imdb.mat');
features = load('/lustre/koustav/BMVC_15/structures/Fisher/features.mat');
ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
TUBNames = data.imdb.images.name;
TuBerlinFeatures = features.descrs;TuBerlinFeatures=TuBerlinFeatures';
mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
%ImageDataPath = '/lustre/koustav/BMVC_15/structures/BOW/';
%ImageData=load([ImageDataPath 'imdb.mat']);
%CaltechNames = ImageData.FileNames;
%CaltechFeatures = ImageData.Features;

for i=1:size(mapping,1)
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end

for j=1:size(TUBNames,2)
    if ~isempty(findstr(TUBNames{j},TUBLabel))
        SketchLabels=[SketchLabels;i];
        SketchFeatures=[SketchFeatures;TuBerlinFeatures(j,:)];
    end
end

end

%save('/lustre/koustav/BMVC_15/structures/Fisher/ImageData.mat','ImageFeatures','ImageLabels','-v7.3');
save('/lustre/koustav/BMVC_15/structures/Fisher/SketchData.mat','SketchFeatures','SketchLabels','-v7.3');
