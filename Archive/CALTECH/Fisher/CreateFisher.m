clc;clear all;
% %data = load('/lustre/koustav/BMVC_15/structures/Fisher/imdb.mat');
% features = load('/lustre/koustav/BMVC_15/structures/Fisher/features.mat');
% ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
% TUBNames = data.imdb.images.name;
% TuBerlinFeatures = features.descrs;TuBerlinFeatures=TuBerlinFeatures';
% mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
% ImageDataPath = '/lustre/koustav/BMVC_15/structures/BOW/';
% ImageData=load([ImageDataPath 'imdb.mat']);
% CaltechNames = ImageData.FileNames;
% CaltechFeatures = ImageData.Features;
% 
% for i=1:size(mapping,1)
%     C=strsplit(mapping{i},':');
%     CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end
% 
% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TuBerlinFeatures(j,:)];
%     end
% end
% 
% end
% 
% save('/lustre/koustav/BMVC_15/structures/Fisher/ImageData.mat','ImageFeatures','ImageLabels','-v7.3');
% save('/lustre/koustav/BMVC_15/structures/Fisher/SketchData.mat','SketchFeatures','SketchLabels','-v7.3');


ImageData = load('/lustre/koustav/BMVC_15/structures/Fisher/ImageData.mat');
SketchData = load('/lustre/koustav/BMVC_15/structures/Fisher/SketchData.mat');
ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;
SketchFeatures = ReduceDim(SketchData.SketchFeatures,1000);
SketchLabels = SketchData.SketchLabels;
idx1=randperm(size(ImageLabels,1));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);


partitions = getPartition(SketchLabels,5);
SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{4})];
SketchLabelsTest = SketchLabels(partitions{5});
SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
SketchFeaturesTest = SketchFeatures(partitions{5},:);
idx2=randperm(size(SketchLabelsTrain,1));
SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);

% %[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain(1:1200,:),ImageFeatures(1:1200,:),SketchLabelsTrain(1:1200,:),ImageLabels(1:1200,:));
[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain,ImageLabels);
SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
save('/lustre/koustav/BMVC_15/structures/Fisher/SketchTestFeaturesProjected.mat','SketchTestFeaturesProjected','SketchLabelsTest');
save('/lustre/koustav/BMVC_15/structures/Fisher/ImageSampleFeaturesProjected.mat','ImageSampleFeaturesProjected','ImageLabels');
