clc;clear all;
FileNames = {};Features =[];Index = 0;
DataSetPath = '/home/cvit/koustav/Datasets/caltech256_features/dense_bow/oneForAll_nr1_K1000/';
ClassDirList = dir(DataSetPath);ClassDirList = {ClassDirList.name};ClassDirList =ClassDirList(3:end);
for i =1: size(ClassDirList,2)
   FileList = dir(strcat(DataSetPath,ClassDirList{i},'/*colorstacked.dense_sift.mat'));FileList = {FileList.name};
   for j = 1:size(FileList,2)
       Index = Index + 1;
       FileNames{Index} = strcat(ClassDirList{i},FileList{j});
       Features(Index,:) = CreateBOW(strcat(DataSetPath,ClassDirList{i},'/',FileList{j}),1000);
   end
end
save('/lustre/koustav/BMVC_15/structures/BOW/imdb.mat','FileNames','Features');