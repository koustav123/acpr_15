clc;clear all;
ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
CaltechFeatures = [];CaltechNames ={};
mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
datapath = '/lustre/koustav/BMVC_15/structures/HOGFeatures/caltech/';
hogdir = dir([datapath '*.mat']);hogdir = {hogdir.name};
% for i =1 : size(hogdir,2)
%     disp(hogdir{i});
%     data = load([datapath hogdir{i}]);
%     CaltechFeatures = [CaltechFeatures; data.features];
%     CaltechNames = [CaltechNames data.labels];
% end

for i=1:size(mapping,1)
    disp(mapping{i});
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
for j=1:size(hogdir,2)
    if ~isempty(findstr(hogdir{j},CalLabel))
        data = load([datapath hogdir{j}]);
        ImageLabels=[ImageLabels;i*ones(size(data.features,1),1)];
        ImageFeatures=[ImageFeatures;data.features];
    end
end

% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
%     end
% end

end


% for i=1:size(mapping,1)
%     disp(mapping{i});
%     C=strsplit(mapping{i},':');
%     CalLabel = C{1};TUBLabel = C{2};
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels;i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end
% 
% % for j=1:size(TUBNames,2)
% %     if ~isempty(findstr(TUBNames{j},TUBLabel))
% %         SketchLabels=[SketchLabels;i];
% %         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
% %     end
% % end
% 
% end

save('/lustre/koustav/BMVC_15/structures/Caltech/HOG_ImageData.mat','ImageFeatures','ImageLabels','-v7.3');

