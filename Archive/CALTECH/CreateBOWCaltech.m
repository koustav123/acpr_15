clc;clear all;
ImageLabels = [];ImageFeatures = [];SketchLabels = [];SketchFeatures = [];
%SketchDataPath = '/home/cvit/koustav/Datasets/howDoHumansSkOFeaturesBOW/features_matlab/';
%TUBData = load([SketchDataPath 'features_shog_hard.mat']);
%TUBerlinFeatures = TUBData.A(:,2:end);TUBLabels = TUBData.A(:,1);
mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
%fileID = fopen('/home/cvit/koustav/Datasets/howDoHumansSkOFeaturesBOW/features_matlab/map_id_label.txt');
%SketchClassIndex = textscan(fileID,'%f %s','Delimiter',',');
%fclose(fileID);
ImageDataPath = '/lustre/koustav/BMVC_15/structures/BOW/';
ImageData=load([ImageDataPath 'imdb.mat']);
CaltechNames = ImageData.FileNames;
CaltechFeatures = ImageData.Features;
% for k =1:size(TUBLabels)
%     TUBNames{k} = SketchClassIndex{1,2}{TUBLabels(k),1};
% end
for i=1:size(mapping,1)
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
for j=1:size(CaltechNames,2)
    if ~isempty(findstr(CaltechNames{j},CalLabel))
        ImageLabels=[ImageLabels;i];
        ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
    end
end

% for j=1:size(TUBNames,2)
%     if ~isempty(findstr(TUBNames{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
%     end
% end

end

save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
%save('/lustre/koustav/BMVC_15/structures/BOW/SketchData.mat','SketchFeatures','SketchLabels');
