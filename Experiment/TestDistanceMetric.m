data = load('TempData.mat');
IF = data.ImageSampleFeaturesProjected;
IL=data.ImageLabels;
SF=data.SketchTestFeaturesProjected;
SL=data.SketchLabelsTest;

PR=[];RE=[];
DM = cell({'euclidean','seuclidean','cityblock','chebychev','minkowski','mahalanobis','cosine','correlation','spearman','hamming','jaccard'});
for i = 1:size(DM,2)
    [PR(i,:),RE(i,:)] = retrieval_1(IF,IL,SF,SL,DM{i});
end