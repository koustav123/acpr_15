#!/bin/bash

#sbatch a.sh "Caltech" "DS" "DS"
#sbatch a.sh 'Caltech' 'DS' 'HOG'
#sbatch a.sh 'Caltech' 'DS' 'Fisher'
#sbatch a.sh 'Caltech' 'HOG' 'DS'
#sbatch a.sh 'Caltech' 'HOG' 'HOG'
#sbatch a.sh 'Caltech' 'HOG' 'Fisher'
#sbatch a.sh 'Caltech' 'CNN' 'CNN'

sbatch a.sh 'Pascal' 'DS' 'HOG'
sbatch a.sh 'Pascal' 'DS' 'DS'
sbatch a.sh 'Pascal' 'DS' 'Fisher'
sbatch a.sh 'Pascal' 'HOG' 'DS'
sbatch a.sh 'Pascal' 'HOG' 'HOG'
sbatch a.sh 'Pascal' 'HOG' 'Fisher'
sbatch a.sh 'Pascal' 'CNN' 'CNN'
