#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 10
#SBATCH -p cvit
#SBATCH --mem=40000
#SBATCH -t 72:00:00

echo $1 $2 $3
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment_5();"
