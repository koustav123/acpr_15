function [] = Experiment_4( Path, dataset ,ImageDescriptor,SketchDescriptor)
addpath('../Utilities/');
%This function finds a precison-recall for the features without doing CCA.
%For unequal feature length, it uses PCA to level the diemsion. Currentlt
%hardcoded for FISHER-DS and DS-HOG


SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels';
if (1 == strcmp(SketchDescriptor,'Fisher') && strcmp(ImageDescriptor,'DS'))
    SketchFeatures = ReduceDim(SketchFeatures,1000);
end
if (1 == strcmp(SketchDescriptor,'DS') && strcmp(ImageDescriptor,'HOG'))
   ImageFeatures = ReduceDim(ImageFeatures,501);
end

% if (1 == strcmp(SketchDescriptor,'Fisher'))
%     SketchFeatures = ReduceDim(SketchData.SketchFeatures,1000);
% else
%     SketchFeatures = SketchData.SketchFeatures;
% end

%ImageFeatures = ReduceDim(ImageFeatures,100);
%SketchFeatures = ReduceDim(SketchFeatures,100);

if (size(SketchData.SketchFeatures,2)>5000 )
    SketchFeatures = ReduceDim(SketchFeatures,1000);
end

if (size(ImageFeatures,2)>5000 )
    ImageFeatures = ReduceDim(ImageFeatures,1000);
end

if (1 == strcmp(ImageDescriptor,'DS'))
    ImageFeatures = ReduceDim(ImageFeatures,501);
end


idx1=randperm(size(ImageLabels,2));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
partitions = getPartition(SketchLabels,5);

for i =1:5
    disp('Cross Validating, Iteration : ');
    disp(i)
    if (i ==1)
      SketchLabelsTrain = [SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{1});
      SketchFeaturesTrain = [SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{1},:);
   elseif (i ==2)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{2});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{2},:);
   elseif (i == 3)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{3});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{3},:);
   elseif (i == 4)
       SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{4});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{4},:);
   else
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4})];
      SketchLabelsTest = SketchLabels(partitions{5});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
      SketchFeaturesTest = SketchFeatures(partitions{5},:);
   end
   idx2=randperm(size(SketchLabelsTrain,2));
   SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);
   [PrecisionBefore(i,:), RecallBefore(i,:)] = retrieval(ImageFeatures,ImageLabels,SketchFeaturesTest,SketchLabelsTest);
   %[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain',ImageLabels');
   %SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
   %ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
   %[PrecisionAfter(i,:),RecallAfter(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchLabelsTest);

end
   % AP = mean(PrecisionAfter,1); AR = mean(RecallAfter,1);
    APB = mean(PrecisionBefore,1);ARB = mean(RecallBefore,1);
    %csvwrite(['/lustre/koustav/BMVC_15/Results/' 'PR_After' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[AP;AR]);
    csvwrite(['/lustre/koustav/BMVC_15/Results/' 'PR_Before' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[APB;ARB]);

end

