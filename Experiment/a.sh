#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 10
#SBATCH -p cvit
#SBATCH --mem=40000
#SBATCH -t 72:00:00

echo $1 $2 $3
#/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r $1
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment('/lustre/koustav/BMVC_15/structures/SelectedFeatures/','$1','$2','$3');"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment('/lustre/koustav/BMVC_15/structures/SelectedFeatures/','$1','$2','$3');"
