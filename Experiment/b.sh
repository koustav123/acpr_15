#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 10
#SBATCH -p long
#SBATCH --mem=60000
#SBATCH -t 24:00:00

echo $1 $2 $3 $4
#/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r $1
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment_1('/lustre/koustav/structures/SelectedFeatures/','$1','$2','$3','$4');"
#LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1 /scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "Experiment('/lustre/koustav/BMVC_15/structures/SelectedFeatures/','$1','$2','$3');"
