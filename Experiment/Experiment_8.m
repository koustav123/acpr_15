%function [] = Experiment_7( Path, dataset ,ImageDescriptor,SketchDescriptor)
clc;clear;
Path = '/lustre/koustav/structures/SelectedFeatures/';
dataset = 'Caltech';
ImageDescriptor = 'CNN';
SketchDescriptor = 'CNN';
addpath('../Utilities/');
%PARTITIONDATA Summary of this function goes here
%   Detailed explanation goes here

ImageData = load([Path dataset '/' dataset '_' ImageDescriptor '_' 'Selected_Data.mat']);
SketchData = load([Path dataset '/' 'TUB' '_' SketchDescriptor '_' 'Selected_Data.mat']);

ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;
%ImageAttached = ImageData.Attached;

SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;
%SketchAttached = SketchData.Attached;
if (1 == strcmp(SketchDescriptor,'Fisher'))
    SketchFeatures = ReduceDim(SketchData.SketchFeatures,1000);
else
    SketchFeatures = SketchData.SketchFeatures;
end

%ImageFeatures = ReduceDim(ImageFeatures,100);
%SketchFeatures = ReduceDim(SketchFeatures,100);

if (size(SketchData.SketchFeatures,2)>5000 )
    SketchFeatures = ReduceDim(SketchFeatures,1000);
end

if (size(ImageFeatures,2)>5000 )
    ImageFeatures = ReduceDim(ImageFeatures,1000);
end



idx1=randperm(size(ImageLabels,2));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
%ImageAttached=ImageAttached(idx1);
partitions = getPartition(SketchLabels,5);

for i =1:5
    disp('Cross Validating, Iteration : ');
    disp(i)
    if (i ==1)
      SketchLabelsTrain = [SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{1});
      SketchFeaturesTrain = [SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{1},:);
      %SketchAttachedTrain = [SketchAttached(partitions{2}) SketchAttached(partitions{3}) SketchAttached(partitions{4}) SketchAttached(partitions{5})];
      %SketchAttachedTest = SketchAttached(partitions{1});
   elseif (i ==2)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{2});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{2},:);
      %SketchAttachedTrain = [SketchAttached(partitions{1}) SketchAttached(partitions{3}) SketchAttached(partitions{4}) SketchAttached(partitions{5})];
      %SketchAttachedTest = SketchAttached(partitions{2});
   elseif (i == 3)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{3});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{3},:);
      %SketchAttachedTrain = [SketchAttached(partitions{1}) SketchAttached(partitions{2}) SketchAttached(partitions{4}) SketchAttached(partitions{5})];
      %SketchAttachedTest = SketchAttached(partitions{3});
   elseif (i == 4)
       SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{4});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{4},:);
      %SketchAttachedTrain = [SketchAttached(partitions{1}) SketchAttached(partitions{2}) SketchAttached(partitions{33}) SketchAttached(partitions{5})];
      %SketchAttachedTest = SketchAttached(partitions{4});
   else
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4})];
      SketchLabelsTest = SketchLabels(partitions{5});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
      SketchFeaturesTest = SketchFeatures(partitions{5},:);
      %SketchAttachedTrain = [SketchAttached(partitions{1}) SketchAttached(partitions{2}) SketchAttached(partitions{3}) SketchAttached(partitions{4})];
      %SketchAttachedTest = SketchAttached(partitions{5});
   end
   idx2=randperm(size(SketchLabelsTrain,2));
   SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);
   [Wx,Wy]=D_CCA(ImageFeatures,ImageLabels,SketchFeaturesTrain,SketchLabelsTrain);
   SketchTestFeaturesProjected = SketchFeaturesTest*Wy;
   ImageSampleFeaturesProjected = ImageFeatures*Wx;
   [Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchLabelsTest);
    
end
     AP = mean(Precision,1); AR = mean(Recall,1);
    csvwrite(['/lustre/koustav/BMVC_15/Results/' 'Files_' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[AP;AR]);

%end

