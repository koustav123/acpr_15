% Retrieve Unknown classes %
clc;clear;
dataset = 'Caltech';
ImageDescriptor = 'DS';
SketchDescriptor = 'Fisher';
Path = '/lustre/koustav/BMVC_15/structures/SelectedFeatures/';

addpath('../Utilities/');
%PARTITIONDATA Summary of this function goes here
%   Detailed explanation goes here

ImageData = load([Path dataset '/' dataset '_' ImageDescriptor '_' 'Selected_Data.mat']);
SketchData = load([Path dataset '/' 'TUB' '_' SketchDescriptor '_' 'Selected_Data.mat']);

ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;


%SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;

if (1 == strcmp(SketchDescriptor,'Fisher'))
    SketchFeatures = ReduceDim(SketchData.SketchFeatures,1000);
else
    SketchFeatures = SketchData.SketchFeatures;
end

%ImageFeatures = ReduceDim(ImageFeatures,100);
%SketchFeatures = ReduceDim(SketchFeatures,100);

if (size(SketchData.SketchFeatures,2)>5000 )
    SketchFeatures = ReduceDim(SketchFeatures,1000);
end

if (size(ImageFeatures,2)>5000 )
    ImageFeatures = ReduceDim(ImageFeatures,1000);
end



idx1=randperm(size(ImageLabels,2));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
for i = 1:1
   tIdx = randi(size(unique(ImageLabels),2),[5 1]);
   disp(tIdx);
  % ImageTrainFeatures = [];ImageTrainLabels = [];
    y = logical(zeros(size(ImageLabels)));
    z = logical(zeros(size(SketchLabels)));
   for j = 1:size(tIdx,1)
       y = bitor(y,ImageLabels ==tIdx(j));
       z = bitor(z,SketchLabels ==tIdx(j));
   end
   ImageTestFeatures = ImageFeatures(y,:);ImageTestLabels = ImageLabels(y);
   ImageTrainFeatures = ImageFeatures(~y,:);ImageTrainLabels = ImageLabels(~y);
   SketchTestFeatures = SketchFeatures(z,:);SketchTestLabels = SketchLabels(z);
   SketchTrainFeatures = SketchFeatures(~z,:);SketchTrainLabels = SketchLabels(~z);
   [Wx,Wy]=clusterCCA_1(SketchTrainFeatures,ImageTrainFeatures,SketchTrainLabels,ImageTrainLabels);
    %SketchTestFeaturesProjected = (Wx' * SketchTestFeatures')';
    SketchTestFeaturesProjected = (Wx' * SketchTrainFeatures(1:100,:)')';
    ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
    %[Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchTestLabels);
    [Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchTrainLabels(1:100,:));

   
end

AP = mean(Precision,1); AR = mean(Recall,1);
csvwrite(['/lustre/koustav/BMVC_15/Results/' 'PR_UNKNOWN_CLASS_Overfit' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[AP;AR]);



