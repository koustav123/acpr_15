function [] = Experiment_5()
addpath('../Utilities/');
%This function tests the effect of concatenating two features, HOG and CNN
%and training them with CCA simultaneously.
%   

ImageData = load('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/Caltech_Concat_HOG_CNN_Selected_Data.mat');
SketchData = load('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/TUB_Concat_HOG_CNN_Selected_Data.mat');

ImageFeatures = ImageData.ImageFeatures;
ImageLabels = ImageData.ImageLabels;


SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;


%ImageFeatures = ReduceDim(ImageFeatures,100);
%SketchFeatures = ReduceDim(SketchFeatures,100);

if (size(SketchData.SketchFeatures,2)>5000 )
    SketchFeatures = ReduceDim(SketchFeatures,1000);
end

if (size(ImageFeatures,2)>5000 )
    ImageFeatures = ReduceDim(ImageFeatures,1000);
end



idx1=randperm(size(ImageLabels,2));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
partitions = getPartition(SketchLabels,5);

for i =1:5
    disp('Cross Validating, Iteration : ');
    disp(i)
    if (i ==1)
      SketchLabelsTrain = [SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{1});
      SketchFeaturesTrain = [SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{1},:);
   elseif (i ==2)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{3}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{2});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{2},:);
   elseif (i == 3)
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{4}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{3});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{3},:);
   elseif (i == 4)
       SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{4});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{4},:);
   else
      SketchLabelsTrain = [SketchLabels(partitions{1}) SketchLabels(partitions{2}) SketchLabels(partitions{3}) SketchLabels(partitions{4})];
      SketchLabelsTest = SketchLabels(partitions{5});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
      SketchFeaturesTest = SketchFeatures(partitions{5},:);
   end
   idx2=randperm(size(SketchLabelsTrain,2));
   SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);
   [Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain',ImageLabels');
   SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
   ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
   [Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchLabelsTest);

end
    AP = mean(Precision,1); AR = mean(Recall,1);
    csvwrite(['/lustre/koustav/BMVC_15/Results/' 'PR_' 'Caltech_2000_Concat' '.csv'],[AP;AR]);

end

