#!/bin/bash

#sbatch b.sh "Caltech" "DS" "DS" "1000"
#sbatch b.sh 'Caltech' 'DS' 'HOG' "1000"
sbatch b.sh 'Caltech' 'DS' 'Fisher' "1000"
#sbatch b.sh 'Caltech' 'HOG' 'DS' "1000"
#sbatch b.sh 'Caltech' 'HOG' 'HOG' "1000"
sbatch b.sh 'Caltech' 'HOG' 'Fisher' "1000"
#sbatch b.sh 'Caltech' 'CNN' 'CNN'

#sbatch b.sh 'Pascal' 'DS' 'HOG'
#sbatch b.sh 'Pascal' 'DS' 'DS'
#sbatch b.sh 'Pascal' 'DS' 'Fisher'
#sbatch b.sh 'Pascal' 'HOG' 'DS'
#sbatch b.sh 'Pascal' 'HOG' 'HOG'
#sbatch b.sh 'Pascal' 'HOG' 'Fisher'
#sbatch b.sh 'Pascal' 'CNN' 'CNN'
