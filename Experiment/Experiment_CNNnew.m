function [AP,AR] = Experiment( Path, dataset ,ImageDescriptor,SketchDescriptor,PCA_DIM)
addpath('../Utilities/');
%same as experiment except that csv is not written and cross validation
%doesn't run. Trains on first four folds and tests on 5th fold
%

ImageData = load([Path dataset '/' dataset '_' ImageDescriptor '_' 'Selected_Data.mat']);
SketchData = load([Path dataset '/' 'TUB' '_' SketchDescriptor '_' 'Selected_Datafc2.mat']);
PCA_DIM = str2num(PCA_DIM);
ImageFeatures = ImageData.ImageFeatures;

ImageLabels = ImageData.ImageLabels;

SketchFeatures = SketchData.SketchFeatures;
SketchLabels = SketchData.SketchLabels;

%if (1 == strcmp(SketchDescriptor,'Fisher'))
 %   SketchFeatures = ReduceDim(SketchData.SketchFeatures,PCA_DIM);
    %SketchFeatures = SketchFeatures(:,1:10);
%else
 %   SketchFeatures = SketchData.SketchFeatures;
%end

if (size(SketchData.SketchFeatures,2)>1000 )
    SketchFeatures = ReduceDim(SketchFeatures,PCA_DIM);
    %SketchFeatures = SketchFeatures(:,1:100);
end

if (size(ImageFeatures,2)>1000 )
    ImageFeatures = ReduceDim(ImageFeatures,PCA_DIM);
    %ImageFeatures = ImageFeatures(:,1:100);
end



idx1=randperm(size(ImageLabels,2));
ImageFeatures = ImageFeatures(idx1,:);ImageLabels = ImageLabels(idx1);
partitions = getPartition(SketchLabels,5);

for i =1:5
    disp('Cross Validating, Iteration : ');
    disp(i)
    if (i ==1)
      SketchLabelsTrain = [SketchLabels(partitions{2}); SketchLabels(partitions{3}); SketchLabels(partitions{4}); SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{1});
      SketchFeaturesTrain = [SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{1},:);
   elseif (i ==2)
      SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{3});SketchLabels(partitions{4});SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{2});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{2},:);
   elseif (i == 3)
      SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{2});SketchLabels(partitions{4});SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{3});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{4},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{3},:);
   elseif (i == 4)
       SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{5})];
      SketchLabelsTest = SketchLabels(partitions{4});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{5},:)];
      SketchFeaturesTest = SketchFeatures(partitions{4},:);
   else
      SketchLabelsTrain = [SketchLabels(partitions{1});SketchLabels(partitions{2});SketchLabels(partitions{3});SketchLabels(partitions{4})];
      SketchLabelsTest = SketchLabels(partitions{5});
      SketchFeaturesTrain = [SketchFeatures(partitions{1},:);SketchFeatures(partitions{2},:);SketchFeatures(partitions{3},:);SketchFeatures(partitions{4},:)];
      SketchFeaturesTest = SketchFeatures(partitions{5},:);
   end
   idx2=randperm(size(SketchLabelsTrain,1));
   SketchFeaturesTrain = SketchFeaturesTrain(idx2,:);SketchLabelsTrain = SketchLabelsTrain(idx2);
   [Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeatures,SketchLabelsTrain,ImageLabels');
   SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
   ImageSampleFeaturesProjected = (Wy' * ImageFeatures')';
  % save('TempData.mat','ImageSampleFeaturesProjected','ImageLabels','SketchTestFeaturesProjected','SketchLabelsTest','-v7.3');
   [Precision(i,:),Recall(i,:)] = retrieval(ImageSampleFeaturesProjected,ImageLabels,SketchTestFeaturesProjected,SketchLabelsTest);

end
    AP = mean(Precision,1); AR = mean(Recall,1);
    csvwrite(['~/results/PreRec/' 'PR_' dataset '_' ImageDescriptor '_' SketchDescriptor '.csv'],[AP;AR]);

end

