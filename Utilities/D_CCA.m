function [ Wx, Wy ] = D_CCA( X,XLabels,Y,YLabels )
%D_CCA It implements the CCA as provided by Ognjen Arandjelovic in
% "Discriminative extended canonical correlation analysis for pattern set
%matching" Journal : Machine Learning 

% First find two principal subspaces Bx and By

% Then find Tx and Ty. Tx and Ty are square root of the full covariance
% matrix, after whitening

% Next find the Normalized inter-class and intra-class scatter matrix for
% data.

% Finally calculate Wx and Wy and return


%X = ImageData.ImageFeatures;
X(X < 0) = 0;
%XLabels = ImageData.ImageLabels;
%Y = SketchData.SketchFeatures;
Y(Y < 0) = 0;
%YLabels = SketchData.SketchLabels;

[Bx,SC1,EIGX] = pca(X);
[By,SC2,EIGY] = pca(Y);
Tx = Bx*whiten(sqrtm(diag(EIGX)))*Bx';
Ty = By*whiten(sqrtm(diag(EIGY)))*By';
% Cx = cov(X);
% Cy = cov(Y);
% [Ex, mu, invMat, whMat] = whiten(Cx);   
% [U1,S1,V1] = svd(Ex);
% SQS1 = sqrt(S1);
% Tx = U1*SQS1*V1;
% 
% [Ey, mu, invMat, whMat] = whiten(Cy);
% [U2,S2,V2] = svd(Ey);
% SQS2 = sqrt(S2);
% Ty = U2*SQS2*V2;

Sb = findScatterMatrix(X,XLabels,Y,YLabels,'inter');
Sw = findScatterMatrix(X,XLabels,Y,YLabels,'intra');
P = sqrtm(Sw)*pinv(Sb)*sqrtm(Sw);
Phi = Tx*P*Ty;
[Vecs,Scores,Eigs] = pca(Phi);
Wx = Bx*Bx'*Vecs;
Wy = By*By'*Vecs;
end

