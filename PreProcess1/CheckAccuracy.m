% run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup'); 
% ImageData = load('/lustre/koustav/Datasets/caltech80.mat');
% Histograms = load('/lustre/koustav/histogram.mat');
% ImageLabels = ImageData.imdb.images.labels;
% Hist = Histograms.hists;
% 
% Training.samples = {};
% Training.labels = {};
% Testing.samples = {};
% Testing.labels  = {};
% 
for classIndex = 1:256
    ims = Hist(ImageLabels == classIndex);
    [TrSamples, idx] = vl_colsubset(ims,60);
    TstSamples = ims(setdiff(1:length(ims),idx));
    Training.samples = [Training.samples TrSamples];
    Testing.samples = [Testing.samples TstSamples];
    Training.labels = [Training.labels num2cell(classIndex*ones(1,length(TrSamples)))];
    Testing.labels = [Testing.labels num2cell(classIndex*ones(1,length(TstSamples)))];
    
end

psix = vl_homkermap(cell2mat(Training.samples), 1, 'kchi2', 'gamma', .5) ;
conf.modelPath = '/lustre/koustav/SVMmodel.mat';
conf.clobber = false ;
conf.svm.C = 10 ;
conf.svm.solver = 'sdca' ;
%conf.svm.solver = 'sgd' ;
%conf.svm.solver = 'liblinear' ;
conf.svm.biasMultiplier = 1 ;

if ~exist(conf.modelPath) || conf.clobber
  switch conf.svm.solver
    case {'sgd', 'sdca'}
      lambda = 1 / (conf.svm.C *  length(Training.samples)) ;
      w = [] ; b =[];
      parfor ci = 1:256
        %perm = randperm(length(selTrain)) ;
        fprintf('Training model for class %d \n', ci) ;
        y = 2 * (cell2mat(Training.labels) == ci) - 1 ;
        [w(:,ci) b(ci) info] = vl_svmtrain((cell2mat(Training.samples)),y, lambda, ...
          'Solver', conf.svm.solver, ...
          'MaxNumIterations', 50/lambda, ...
          'BiasMultiplier', conf.svm.biasMultiplier, ...
          'Epsilon', 1e-3);
      end

%     case 'liblinear'
%       svm = train(imageClass(selTrain)', ...
%                   sparse(double(psix(:,selTrain))),  ...
%                   sprintf(' -s 3 -B %f -c %f', ...
%                           conf.svm.biasMultiplier, conf.svm.C), ...
%                   'col') ;
%       w = svm.w(:,1:end-1)' ;
%       b =  svm.w(:,end)' ;
  end

  model.b = conf.svm.biasMultiplier * b ;
  model.w = w ;

  save(conf.modelPath, 'model') ;
else
  load(conf.modelPath) ;
end


% Testing Starts

% scores = model.w' * cell2mat(Testing.samples) + model.b' * ones(1,size(cell2mat(Testing.samples),2)) ;
% [drop, imageEstClass] = max(scores, [], 1) ;
% 
% Compute the confusion matrix
% idx1 = sub2ind([256, 256], ...
%               cell2mat(Testing.labels), imageEstClass) ;
% confus = zeros(256) ;
% confus = vl_binsum(confus, ones(size(idx1)), idx1) ;
% 
% Plots
% figure(1) ; clf;
% subplot(1,2,1) ;
% imagesc(scores) ; title('Scores') ;
% set(gca, 'ytick', 1:256, 'yticklabel', unique(ImageData.imdb.images.labels)) ;
% subplot(1,2,2) ;
% imagesc(confus) ;
% title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
%               100 * mean(diag(confus)/20) )) ;



