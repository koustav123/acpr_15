clc;clear all;
mapping=importdata('/home/cvit/koustav/code/mapping_CCA.txt');
ClassMapping = importdata('/home/cvit/koustav/code/caltechmappingkoustavsir.txt');
ImageData = load('/lustre/koustav/Datasets/caltech80.mat');
CaltechLabels = ImageData.imdb.images.labels;
imdb.images.labels = [];
imdb.images.names = {};
imdb.images.data = [];
for i = 1: numel(mapping)
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (ImageData.imdb.images.labels == CL);
    imdb.images.labels = [imdb.images.labels ImageData.imdb.images.labels(idx)];
    imdb.images.names = [imdb.images.names ImageData.imdb.images.names(idx)];
    imdb.images.data = cat(4, imdb.images.data, ImageData.imdb.images.data(:,:,:,idx));
end
save('/lustre/koustav/caltech105_80.mat','imdb','-v7.3');