%function [] = ExtractCaltechDsift()
%EXTRACTCALTECHDSIFT Summary of this function goes here
%   Detailed explanation goes here

ImageDataBase = load('/lustre/koustav/Datasets/caltech105_80.mat');
%conf.calDir = 'data/caltech-101' ;
descrs = {};
conf.phowOpts = {'Verbose', 2, 'Sizes', 7, 'Step', 5} ;
conf.numSpatialX = [2 4] ;
conf.numSpatialY = [2 4] ;
conf.numWords = 300;
conf.quantizer = 'kdtree' ;

model.phowOpts = conf.phowOpts ;
model.numSpatialX = conf.numSpatialX;
model.numSpatialY = conf.numSpatialY;
model.quantizer = conf.quantizer;
model.numWords = conf.numWords;
model.vocab = [] ;


ImageData = ImageDataBase.imdb.images.data;
ImageLabels = ImageDataBase.imdb.images.names;
%for i =1:numel(ImageData(1,1,1,:))
for i = 1:10
    im = standarizeImage(ImageData(:,:,:,i));
    [drop, descrs{i}] = vl_phow(im,model.phowOpts{:});
    %imshow(im);
end
descrs = vl_colsubset(cat(2, descrs{:}), 10e4) ;
descrs = single(descrs) ;
vocab = vl_kmeans(descrs, conf.numWords, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50);
model.vocab = vocab;
model.vocab = vocab ;
if strcmp(model.quantizer, 'kdtree')
  model.kdtree = vl_kdtreebuild(vocab) ;
end

hists = {};
for ii =1:numel(ImageData(1,1,1,:))
%for ii = 1:10
   im = standarizeImage(ImageData(:,:,:,ii));
   hists{ii} = getImageDescriptor(model, im);
end
%end
save('/lustre/koustav/histograms_caltech105_80.mat', 'hists','-v7.3') ;
% 
% 
% 
