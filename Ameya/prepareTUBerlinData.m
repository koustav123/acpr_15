clear;clc;

% get all folders
folders = dir('/lustre/koustav/Datasets/tu_berlin105/');
% remove . and .. directories from listing
folders(1:2) = []; 

% get class names
classNames = {folders.name};
numClasses = length(classNames);
classes = cell(1,numClasses);

% total number of images is 116130
names = cell(1,8400);
data = zeros(220,220,1,8400,'uint8');
labels = zeros(1,8400,'uint16');
sets = ones(1,8400,'uint8');
%dataMean=zeros(220,220,1);
k = uint64(1);

for i = 1:numClasses
    files = dir(fullfile('/lustre/koustav/Datasets/tu_berlin105/',classNames{i}));
    files(1:2) = [];
    
    numImages = length(files);
    
    for j = 1:numImages
        if mod(j,3) == 0
            names{k} = fullfile('/lustre/koustav/Datasets/tu_berlin105/',classNames{i},files(j).name);
            fprintf('%8lu %s\n',[k names{k}]);
            im = imread(names{k});
            im=imresize(im, [220 220] );
            data(:,:,:,k)=im;
            labels(k) = i;
            names{k} = names{k};
            sets(k)=3;
            k=k+1;
        else
            names{k} = fullfile('/lustre/koustav/Datasets/tu_berlin105/',classNames{i},files(j).name);
            fprintf('%8lu %s\n',[k names{k}]);
            im = imread(names{k});
            im=imresize(im, [220 220] );
			data(:,:,:,k)=im;
            labels(k) = i;
            names{k} = names{k};            
            k = k+1;
        end
    end   
    classes{i} = folders(i).name;
end
disp(k);

clear className classNames files folders i j k numClasses numImages image imval
%   data=single(data(:,:,:,:));
  
%   data = bsxfun(@minus, data, dataMean);
% 
% % normalize by image mean and std as suggested in `An Analysis of
% % Single-Layer Networks in Unsupervised Feature Learning` Adam
% % Coates, Honglak Lee, Andrew Y. Ng
% 
%  % Performing Contrast Normalization
%    z = reshape(data,[],48400) ; %TODO: Check this 60000. Most probably its wrong.
%    z = bsxfun(@minus, z, mean(z,1)) ;
%    n = std(z,0,1) ;
%   z = bsxfun(@times, z, mean(n) ./ max(n, 40)) ;
%   data = reshape(z, 220, 220, 1, []) ;
% clear z n
% 
%  %Performing ZCA Whitening of Data
%  
%    z = single(reshape(data,[],48400)) ; %TODO: Check this 60000.
%    W = z(:,sets == 1)*z(:,sets == 1)'/48400 ; %TODO: Check this 60000.
%    [V,D] = eig(W) ;
%    %the scale is selected to approximately preserve the norm of W
%    d2 = diag(D) ;
%    en = sqrt(mean(d2)) ;
%    z = V*diag(en./max(sqrt(d2), 10))*V'*z ;
%    data = reshape(z, 220, 220, 1, []) ;
%    
%  clear z W V D d2 en
 
 dataMean = mean(data(:,:,:,sets == 1), 4);
 
idx=uint32(randperm(8400));
data=data(:,:,:,idx);
sets=sets(idx);
labels=labels(idx);
names=names(idx);

imdb.images.data=data;
imdb.images.set=sets;
imdb.images.data_mean=dataMean;
imdb.images.labels=labels;
imdb.images.names=names;

imdb.meta.classes = classes;
imdb.meta.sets = {'train','val','test'};
save('prepareddata.mat','imdb','-v7.3');
clear names data labels sets classes im l
