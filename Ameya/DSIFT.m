run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup'); 
descrs = {} ;
for i = 1:400
% -------------------------------------------------------------------------
    im=imread('test.png');
    im = im2single(im) ;
    im=imresize(im, [220 220]);
     width = size(im,2) ;
     height = size(im,1) ;
     phowopts={'Verbose', 2, 'Sizes', 7, 'Step', 5} ;
     % get PHOW features
     [drop, descrs{i}] = vl_phow(im, phowopts{:}) ;
%     % quantize local descriptors into visual words
%     switch model.quantizer
%     case 'vq'
%         [drop, binsa] = min(vl_alldist(model.vocab, single(descrs{i})), [], 1) ;
%     case 'kdtree'
%         binsa = double(vl_kdtreequery(model.kdtree, model.vocab, ...
%                                   single(descrs{i}), ...
%                                   'MaxComparisons', 50)) ;
%        end
% 
%     for i = 1:length(model.numSpatialX)
%         binsx = vl_binsearch(linspace(1,width,model.numSpatialX(i)+1), frames(1,:)) ;
%         binsy = vl_binsearch(linspace(1,height,model.numSpatialY(i)+1), frames(2,:)) ;
% 
%         % combined quantization
%         bins = sub2ind([model.numSpatialY(i), model.numSpatialX(i), numWords], ...
%                  binsy,binsx,binsa) ;
%         hist = zeros(model.numSpatialY(i) * model.numSpatialX(i) * numWords, 1) ;
%         hist = vl_binsum(hist, ones(size(bins)), bins) ;
%         hists{i} = single(hist / sum(hist)) ;
%     end
%     hist = cat(1,hists{:}) ;
%     hist = hist / sum(hist) ;
end
% descrs2 =vl_colsubset(cat(2, descrs{:}), 10e4) ;
%descrs2 = single(descrs2) ;
%vocab = vl_kmeans(descrs2, 300, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50) ;
