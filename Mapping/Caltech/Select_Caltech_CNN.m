clc;clear;
ImageLabels = [];ImageFeatures = [];Attached=[];
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/caltechmappingkoustavsir.txt');

ImageDataPath = '/lustre/koustav/structures/FullFeatures/';
ImageData=load([ImageDataPath 'Caltech_CNN_Full_ImageDatafc3.mat']);
CaltechNames = ImageData.FileNames;
CaltechFeatures = ImageData.Features;
%CaltechAttached = ImageData.Attached;
for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures=[ImageFeatures;CaltechFeatures(idx,:)];
    ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];
    %Attached=[Attached CaltechAttached(:,idx)];

end
%save('/lustre/koustav/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/structures/SelectedFeatures/Caltech/Caltech_CNN_Selected_Datafc3.mat','ImageLabels','ImageFeatures','Attached','-v7.3');