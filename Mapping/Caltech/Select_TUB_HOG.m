clc;clear;
SketchLabels = [];SketchFeatures = [];
SketchDataPath = '/lustre/koustav/structures/FullFeatures/';
TUBData = load([SketchDataPath 'TUB_HOG_Full_SketchData.mat']);
TUBerlinFeatures = TUBData.Features;TUBLabels = TUBData.FileNames;
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/TUBerlinmappingkoustavsir.txt');
for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBLabels == CL);
    SketchFeatures = [SketchFeatures ; TUBerlinFeatures(idx,:)];
    SketchLabels=[SketchLabels i*ones(1,sum(idx))];

% for j=1:size(TUBLabels,2)
%     if ~isempty(findstr(TUBLabels{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
%     end
% end

end
SketchLabels = SketchLabels';
save('/lustre/koustav/structures/SelectedFeatures/Caltech/TUB_HOG_Selected_Data.mat','SketchFeatures','SketchLabels','-v7.3');