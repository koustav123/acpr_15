#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 1
#SBATCH -p cvit
#SBATCH --mem=2048
#SBATCH -t 24:00:00

#sbatch a.sh "Select_Caltech_DS" 
#sbatch a.sh "Select_TUB_DS" 
#sbatch a.sh "Select_TUB_Fisher" 
#sbatch a.sh "Select_TUB_HOG"
#sbatch a.sh "Select_Caltech_HOG"
#sbatch a.sh "Select_Caltech_CNN"
#sbatch a.sh "Select_TUB_CNN"
sbatch a.sh "Select_Caltech_Concat"
sbatch a.sh "Select_TUB_Concat"