clc;clear;
SketchLabels = [];SketchFeatures = []; hashes=[];
SketchDataPath = '/lustre/koustav/structures/FullFeatures/';
TUBData = load([SketchDataPath 'TUB_Fisher_Full_SketchData.mat']);
TUBerlinFeatures = TUBData.Features;TUBLabels = TUBData.FileNames;
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/TUBerlinmappingkoustavsir.txt');
hash=zeros(1,20000,'uint32');
for i=1:20000
    hash(i)=i;
end
    
for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBLabels == CL);
    SketchFeatures = [SketchFeatures ; TUBerlinFeatures(idx,:)];
    SketchLabels=[SketchLabels ;i*ones(sum(idx),1)];
    hashes=[hashes hash(idx)];

% for j=1:size(TUBLabels,2)
%     if ~isempty(findstr(TUBLabels{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
%     end
% end

end
save('/lustre/koustav/structures/SelectedFeatures/Caltech/TUB_Fisher_Selected_Data.mat','SketchFeatures','SketchLabels','hashes','-v7.3');
