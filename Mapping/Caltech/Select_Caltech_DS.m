clc;clear all;
ImageLabels = [];ImageFeatures = [];
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/caltechmappingkoustavsir.txt');

%ImageDataPath = '/lustre/koustav/structures/';
%ImageData=load([ImageDataPath 'Caltech_DS_Full_ImageData.mat']);
ImageData = load('/lustre/koustav/structures/NewFeatures/Caltech_105_80_DSIFT_Kernel.mat');
hash = ImageData.ImagePaths;
CaltechNames = ImageData.ImageLabels;
%CaltechNames = CaltechNames;
CaltechFeatures = ImageData.psix;
%ImageLabels = CaltechNames';
%ImageFeatures = CaltechFeatures;

for i=1:size(mapping,1)
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures=[ImageFeatures;CaltechFeatures(idx,:)];
    ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];

end

% %save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/structures/SelectedFeatures/Caltech/Caltech_DS_Selected_Data.mat','ImageLabels','ImageFeatures','hash','-v7.3');
%save('/lustre/koustav/structures/SelectedFeatures/Caltech/Caltech_DS_Selected_Data.mat','CaltechNames','CaltechFeatures','-v7.3');