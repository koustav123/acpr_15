clc;clear all;
SketchLabels = [];SketchFeatures = [];Attached=[];
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/TUBerlinmappingkoustavsir.txt');


SketchDataPath = '/lustre/koustav/structures/FullFeatures/';
SketchData=load([SketchDataPath 'TUB_CNN_Full_SketchDatafc1.mat']);
TUBNames = SketchData.FileNames;
TUBFeatures = SketchData.Features;
TUBAttached = SketchData.Attached;
for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBNames == CL);
    SketchFeatures=[SketchFeatures;TUBFeatures(idx,:)];
    SketchLabels=[SketchLabels; i*ones(size(TUBNames(idx)))];
    %Attached = [Attached TUBAttached(:,idx)];

end
save('/lustre/koustav/structures/SelectedFeatures/Caltech/TUB_CNN_Selected_Datafc1.mat','SketchLabels','SketchFeatures','-v7.3');