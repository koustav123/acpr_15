#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 8
#SBATCH -p cvit
#SBATCH --mem=32000
#SBATCH -t 24:00:00

echo $1
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r $1
