clc;clear all;
ImageLabels = [];ImageFeatures = [];
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/caltechmappingkoustavsir.txt');

ImageDataPath = '/lustre/koustav/structures/FullFeatures/';
ImageData=load([ImageDataPath 'Caltech_HOG_Full_ImageData.mat']);
CaltechNames = ImageData.labels;
CaltechFeatures = ImageData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures=[ImageFeatures;CaltechFeatures(idx,:)];
    ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];
% for j=1:size(CaltechNames,2)
%     if ~isempty(findstr(CaltechNames{j},CalLabel))
%         ImageLabels=[ImageLabels i];
%         ImageFeatures=[ImageFeatures;CaltechFeatures(j,:)];
%     end
% end


end
%save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');
save('/lustre/koustav/structures/SelectedFeatures/Caltech/Caltech_HOG_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');