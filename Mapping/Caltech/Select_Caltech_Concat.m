clc;clear;
addpath('/home/cvit/koustav/code/Utilities');

ImageLabels = [];ImageFeatures1 = [];
mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
ClassMapping = importdata('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/caltechmappingkoustavsir.txt');

ImageDataPath = '/lustre/koustav/BMVC_15/structures/FullFeatures/';
ImageData=load([ImageDataPath 'Caltech_CNN_Full_ImageData.mat']);
CaltechNames = ImageData.FileNames;
CaltechFeatures1 = ImageData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures1=[ImageFeatures1;CaltechFeatures1(idx,:)];
    ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];


end

%ImageLabels = [];
ImageFeatures2 = [];
%mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');

ImageDataPath = '/lustre/koustav/BMVC_15/structures/FullFeatures/';
ImageData=load([ImageDataPath 'Caltech_HOG_Full_ImageData.mat']);
%CaltechNames = ImageData.FileNames;
CaltechFeatures2 = ImageData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(CalLabel,ClassMapping.textdata,'exact');
    idx = (CaltechNames == CL);
    ImageFeatures2=[ImageFeatures2;CaltechFeatures2(idx,:)];
    %ImageLabels=[ImageLabels i*ones(size(CaltechNames(idx)))];


end

ImageFeatures = [ReduceDim(ImageFeatures1,1000) ReduceDim(ImageFeatures2,1000)];
save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/Caltech_Concat_HOG_CNN_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');
%save('/lustre/koustav/BMVC_15/structures/BOW/ImageData.mat','ImageFeatures','ImageLabels');





%save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/Caltech_CNN_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');