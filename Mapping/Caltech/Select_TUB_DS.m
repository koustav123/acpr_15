clc;clear all;
SketchLabels = [];SketchFeatures = [];
SketchDataPath = '/lustre/koustav/structures/FullFeatures/';
TUBData = load([SketchDataPath 'TUB_DS_Full_SketchData.mat']);
TUBerlinFeatures = TUBData.Features;TUBNames = TUBData.TUBLabels;
mapping=importdata('~/code/mapping_CCA.txt');
ClassMapping = importdata('~/code/TUBerlinmappingkoustavsir.txt');

for i=1:size(mapping,1)
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBNames == CL);
    SketchFeatures = [SketchFeatures ; TUBerlinFeatures(idx,:)];
    SketchLabels=[SketchLabels i*ones(1,sum(idx))];

% for j=1:size(TUBLabels,2)
%     if ~isempty(findstr(TUBLabels{j},TUBLabel))
%         SketchLabels=[SketchLabels;i];
%         SketchFeatures=[SketchFeatures;TUBerlinFeatures(j,:)];
%     end
% end

end
SketchLabels = SketchLabels';
save('/lustre/koustav/structures/SelectedFeatures/Caltech/TUB_DS_Selected_Data.mat','SketchFeatures','SketchLabels','-v7.3');
