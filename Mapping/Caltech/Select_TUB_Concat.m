clc;clear all;
addpath('/home/cvit/koustav/code/Utilities');
SketchLabels = [];SketchFeatures1 = [];
mapping=importdata('/lustre/koustav/BMVC_15/structures/Archive/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');
ClassMapping = importdata('/lustre/koustav/BMVC_15/structures/ParameterTuning/Tune_PCA/FullData/TUBerlinmappingkoustavsir.txt');

SketchDataPath = '/lustre/koustav/BMVC_15/structures/FullFeatures/';
SketchData=load([SketchDataPath 'TUB_CNN_Full_SketchData.mat']);
TUBNames = SketchData.FileNames;
TUBFeatures1 = SketchData.Features;

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBNames == CL);
    SketchFeatures1 = [SketchFeatures1 ; TUBFeatures1(idx,:)];
    SketchLabels=[SketchLabels i*ones(size(TUBNames(idx)))];
   

end
%save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/TUB_CNN_Selected_Data160.mat','SketchLabels','SketchFeatures','-v7.3');

%SketchLabels = [];
SketchFeatures2 = [];
SketchDataPath = '/lustre/koustav/BMVC_15/structures/FullFeatures/';
TUBData = load([SketchDataPath 'TUB_HOG_Full_SketchData.mat']);
TUBFeatures2 = TUBData.Features;%TUBLabels = TUBData.FileNames;
%mapping=importdata('/lustre/koustav/BMVC_15/structures/CNNFeatures/sketchBasedRetrieval/mapping_CCA.txt');

for i=1:size(mapping,1)
    disp(i);
    C=strsplit(mapping{i},':');
    CalLabel = C{1};TUBLabel = C{2};
    CL = strmatch(TUBLabel,ClassMapping.textdata,'exact');
    idx = (TUBNames == CL);
    SketchFeatures2 = [SketchFeatures2 ; TUBFeatures2(idx,:)];
    %SketchLabels=[SketchLabels i*ones(size(TUBNames(idx)))];
   
end
SketchFeatures1 = ReduceDim(SketchFeatures1,1000); SketchFeatures2 = ReduceDim(SketchFeatures2,1000);
SketchFeatures = [SketchFeatures1 SketchFeatures2];
save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Caltech/TUB_Concat_HOG_CNN_Selected_Data.mat','SketchLabels','SketchFeatures','-v7.3');