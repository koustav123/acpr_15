clc;clear all;
ImageLabels = [];ImageFeatures = [];
ClassLabels = cell({'airplane','bicycle','flying bird','sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted plant','sheep','stofa','train','tv'});

ImageDataPath = '/lustre/koustav/structures/FullFeatures/';
ImageData=load([ImageDataPath 'Pascal_CNN_Full_ImageData.mat']);
PascalNames = ImageData.FileNames;
PascalFeatures = ImageData.Featurenew;
for i =1: size(ClassLabels,2)
    disp(ClassLabels{i});
    %temp=[];
    if (i == 18)
        disp('Yo');
    end
    for j=1:size(PascalNames,2)
    if ~isempty(findstr(PascalNames{j},ClassLabels{i}))
        ImageLabels=[ImageLabels i];
        ImageFeatures=[ImageFeatures;PascalFeatures(j,:)];
        %temp = [temp;PascalFeatures(j,:)];
    end
    %ImageFeatures=[ImageFeatures;temp];
end

end
save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Pascal/Pascal_CNN_Selected_Data.mat','ImageLabels','ImageFeatures','-v7.3');