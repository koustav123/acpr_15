clc;clear all;
SketchLabels = [];SketchFeatures = [];
ClassLabels = cell({'airplane','bicycle','flying bird','sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted plant','sheep','sofa','train','tv'});

SketchDataPath = '/lustre/koustav/BMVC_15/structures/FullFeatures/';
SketchData=load([SketchDataPath 'TUB_CNN_Full_SketchData.mat']);
TUBNames = SketchData.FileNames;
TUBFeatures = SketchData.Features;
temp={};
for i =1: size(ClassLabels,2)
    disp(ClassLabels{i});disp(size(SketchLabels));
    for j=1:size(TUBNames,2)
    if ~isempty(findstr(TUBNames{j},ClassLabels{i}))
        SketchLabels=[SketchLabels i];
        SketchFeatures=[SketchFeatures;TUBFeatures(j,:)];
    end
end

end
save('/lustre/koustav/BMVC_15/structures/SelectedFeatures/Pascal/TUB_CNN_Selected_Data.mat','SketchLabels','SketchFeatures','-v7.3');