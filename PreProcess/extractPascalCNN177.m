
clc;
clear;

imdb = load('/lustre/koustav/Datasets/177fullfeaturesPascalsingle.mat');
imdb = imdb.imdb;
Features = imdb.images.featuresfc3conv21;
FileNames = imdb.images.labels;
%Attached=imdb.images.names;
%FileNames = FileNames.textdata;FileNames = FileNames';
save('/lustre/koustav/structures/FullFeatures/Pascal_CNN_Full_ImageDatafc3.mat','Features','FileNames','-v7.3');