clc;clear all;
run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup');
FullData = load('/lustre/koustav/Datasets/TUBerlin_105_80_mapped.mat');
Images = FullData.imdb.images.data;
FileNames = FullData.imdb.images.labels;
Features = zeros(size(Images,4),20000);
for i =1: size(Images,4)
    %disp(i);
    Image = Images(:,:,:,i);
    lvl = graythresh(Image);
       Image = imresize(im2bw(Image,lvl),[200 200]);
       hog = vl_hog(single(Image), 8,'numOrientations', 8,'variant', 'dalaltriggs') ;
       Features(i,:) = hog(:);
end
save('/lustre/koustav/structures/FullFeatures/TUB_HOG_Full_SketchData.mat','Features','FileNames','-v7.3');