clc;clear;
run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup'); 
ImageData = load('/lustre/koustav/Datasets/177fullfeaturesPascalsingle.mat');
Images  = ImageData.imdb.images.data;
%ClassLabels = load('/lustre/koustav/BMVC_15/structures/FullData/pascalVOClabels.mat');
ClassLabels = ImageData.imdb.images.labels;
Labels = cell({'airplane','bicycle','flying bird',...
'sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted plant',...
'sheep','sofa','train','tv'});
Features = zeros(6146,20000);
% j=1;
 for i =1: size(Images,4)
%     disp(i);
%     if (size(ClassLabels.labels{i},2)~=1)
%         continue;
%     end
     Image = Images(:,:,:,i);
     lvl = graythresh(Image);
        Image = imresize(im2bw(Image,lvl),[200 200]);
        hog = vl_hog(single(Image), 8,'numOrientations', 8,'variant', 'dalaltriggs') ;
        Features(i,:) = hog(:);
%        FileNames{j} = Labels{str2num(ClassLabels.labels{i}{1})};
%        j=j+1;
 end
 save('/lustre/koustav/structures/FullFeatures/Pascal_HOG_Full_ImageData.mat','Features','ClassLabels','-v7.3');