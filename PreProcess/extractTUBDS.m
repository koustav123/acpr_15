clc;clear all;
SketchDataPath = '/home/cvit/koustav/Datasets/howDoHumansSkOFeaturesBOW/features_matlab/';
TUBData = load([SketchDataPath 'features_shog_hard.mat']);
Features = TUBData.A(:,2:end);TUBLabels = TUBData.A(:,1);
fileID = fopen('/home/cvit/koustav/Datasets/howDoHumansSkOFeaturesBOW/features_matlab/map_id_label.txt');
SketchClassIndex = textscan(fileID,'%f %s','Delimiter',',');
fclose(fileID);
% for k =1:size(TUBLabels)
%     FileNames{k} = SketchClassIndex{1,2}{TUBLabels(k),1};
% end

save('/lustre/koustav/structures/FullFeatures/TUB_DS_Full_SketchData.mat','Features','TUBLabels','-v7.3');

