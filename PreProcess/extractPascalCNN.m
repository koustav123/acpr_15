clc;clear all;
%run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup'); 
Features = load('/lustre/koustav/structures/FullData/pascalVOCfeatures.mat'); Features  = Features.pascalVOCfeatures;
ClassLabels = load('/lustre/koustav/structures/FullData/pascalVOClabels.mat');
Labels = cell({'airplane','bicycle','flying bird','sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted plant','sheep','sofa','train','tv'});
j=1;
Featurenew=zeros(6146,4096);
for i = 1:size(ClassLabels.labels,2)
    disp(i);
    if (size(ClassLabels.labels{i},2)~=1)
        continue;
    end
FileNames{j} = Labels{str2num(ClassLabels.labels{i}{1})};
Featurenew(j,:)=Features(i,:);
j=j+1;
end

save('/lustre/koustav/BMVC_15/structures/FullFeatures/Pascal_CNN_Full_ImageData.mat','Featurenew','FileNames','-v7.3');