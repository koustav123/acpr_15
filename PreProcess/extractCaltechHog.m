clc;clear;
run('/home/cvit/koustav/vlfeat-0.9.20/toolbox/vl_setup'); 
data = load('/lustre/koustav/Datasets/caltech80.mat');
 Images  = data.imdb.images.data;
 labels = data.imdb.images.labels;
% %FileNames = load('/lustre/koustav/BMVC_15/structures/FullData/Caltech256labels.mat');
% FileNames = importdata('/lustre/koustav/BMVC_15/structures/FullData/Caltech256FileList.txt');
% FileNames = FileNames.textdata;
% FileNames = FileNames';
 Features = zeros(size(Images,4),20000);
for i =1: size(Images,4)
    disp(i);
    Image = Images(:,:,:,i);
    lvl = graythresh(Image);
       Image = imresize(im2bw(Image,lvl),[200 200]);
       hog = vl_hog(single(Image), 8,'numOrientations', 8,'variant', 'dalaltriggs') ;
       Features(i,:) = hog(:);
end
save('/lustre/koustav/structures/FullFeatures/Caltech_HOG_Full_ImageData.mat','Features','labels','-v7.3');