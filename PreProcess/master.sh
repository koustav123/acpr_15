#!/bin/bash
#SBATCH -A cvit 
#SBATCH -n 1
#SBATCH -p cvit
#SBATCH --mem=2048
#SBATCH -t 24:00:00

#sbatch a.sh "extractCaltechDS" &
#sbatch a.sh "extractCaltechHog"
sbatch a.sh "extractPascalDS"
sbatch a.sh "extractPascalHog"
sbatch a.sh "extractPascalCNN"
#sbatch a.sh "extractTUBFisher" &
#sbatch a.sh "extractTUBHog" &
#sbatch a.sh "extractTUBDS"&

