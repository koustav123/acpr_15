%clc;clear all;
%data = load('/lustre/koustav/structures/Misc/imdb.mat');
%features = load('/lustre/koustav/structures/Misc/features.mat'); 
Features = data.descrs; Features = Features';
FileNames = labels.imdb.images.class;
save('/lustre/koustav/structures/FullFeatures/TUB_Fisher_Full_SketchData.mat','Features','FileNames','-v7.3');