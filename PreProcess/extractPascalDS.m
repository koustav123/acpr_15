clc;clear;
addpath('../Utilities/');
ImageDataset = 'pascal07';
ImageDataPath = '/home/cvit/koustav/Datasets/pascal07.20100609/';
Labels = cell({'airplane','bicycle','flying bird','sailboat','wine-bottle','bus','car','cat', 'chair', 'cow',...
    'table','dog', 'horse','motorbike', 'person','potted plant','sheep','sofa','train','tv'});
ImageSampleLabels = importdata([ImageDataPath ImageDataset '_train_classes.txt']);

[~,ImageLabels] = max(ImageSampleLabels,[],2);
Features = logical(vec_read([ImageDataPath ImageDataset '_train_DenseSift.hvecs']));
s = sum(ImageSampleLabels,2);
idx = (s == 1);
Features = Features(idx,:);
ImageLabels = ImageLabels(idx);
FileNames = {};
for i =1 :size(ImageLabels,1)
    %disp(i);
    %idx = (s ==1);
    FileNames{i} = Labels{ImageLabels(i)};
end
ImageSampleLabels2 = importdata([ImageDataPath ImageDataset '_test_classes.txt']);

[~,ImageLabels2] = max(ImageSampleLabels2,[],2);
Features2 = logical(vec_read([ImageDataPath ImageDataset '_test_DenseSift.hvecs']));
s = sum(ImageSampleLabels2,2);
idx = (s == 1);
Features2 = Features2(idx,:);
ImageLabels2 = ImageLabels2(idx);
FileNames2 = {};
for i =1 :size(ImageLabels2,1)
    %disp(i);
    %idx = (s ==1);
    FileNames2{i} = Labels{ImageLabels2(i)};
end
Features=[Features;Features2];
FilenamesFinal=[FileNames FileNames2];
save('/lustre/koustav/structures/FullFeatures/Pascal_DS_Full_ImageData.mat','Features','FilenamesFinal','-v7.3');
